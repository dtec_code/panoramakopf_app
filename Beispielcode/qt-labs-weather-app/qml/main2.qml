import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Controls.Styles 1.0
//import "pages"
//import "models"
import "pages"

Rectangle {
    id: rectangle1
    width: 400
    height: 300

    BasicPage {
        x: -142
        y: 110
    }

    SplitView {
        id: splitView1
        x: 8
        y: 8
        width: 384
        height: 284

        Column {
            id: column1
            x: 0
            y: 0
            width: 200
            height: 143
        }
    }
}
