/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.1
import QtQuick.Layouts 1.0
import "../js/utils.js" as Utils
import org.qtproject.demo.weather 1.0
//import "../touch"

GridLayout {
    id: root
    flow: ApplicationInfo.isPortraitMode ? GridLayout.LeftToRight : GridLayout.TopToBottom

    property alias slider: touchSlider
    property QtObject model

    Separator {
        Layout.fillHeight: true
        Layout.fillWidth: ApplicationInfo.isPortraitMode ? true : false
        Layout.minimumHeight: 10
    }
    Canvas {
        id: canvasSlider
        Layout.alignment: Qt.AlignHCenter

        property int drawingOffset: ApplicationInfo.sliderGapWidth
        property var rangeTemp: Utils.getMaxMinTemp(model)

        implicitWidth: touchSlider.width + 2 * drawingOffset
        implicitHeight: 3.8*drawingOffset

        property int marginsTemperaturesDrawing: calibrate(rangeTemp[1]) +  10 * ApplicationInfo.ratio
        property real circleIconWidth: 20 * ApplicationInfo.ratio
        property real weatherIconWidth: 80 * ApplicationInfo.ratio

        function calibrate(temperature) {
            return 2 * ApplicationInfo.ratio * temperature
        }

        antialiasing: true
        smooth: true
        onPaint: {
            var ctx = getContext('2d')
            var count = model.periodCount()
            ctx.save()
            ctx.beginPath();
            ctx.fillStyle = ApplicationInfo.colors.doubleDarkGray
            ctx.lineWidth = 1
            ctx.translate(touchSlider.x, marginsTemperaturesDrawing + weatherIconWidth + 15 * ApplicationInfo.ratio + circleIconWidth/2)
            ctx.moveTo(drawingOffset/2, 0)
            for (var i = 0; i < count; i++) {
                ctx.moveTo((i + .5 )* drawingOffset, 0)
                var temperatureStart = canvasSlider.calibrate(Utils.getTemperature(i, model))
                var temperatureEnd = canvasSlider.calibrate(Utils.getTemperature(i + 1, model))
                ctx.moveTo((i + 1 )* drawingOffset, -temperatureStart)
                if ( (i+1) < count)
                    ctx.lineTo((i + 2) * drawingOffset, -temperatureEnd)
            }
            ctx.stroke()
            ctx.closePath()
            ctx.restore();
        }
        Repeater {
            id: repeater
            model: root.model.periodCount()
            Column {
                x: (index + 1.5) * ApplicationInfo.sliderGapWidth - canvasSlider.weatherIconWidth/2 - canvasSlider.circleIconWidth/2
                y: -canvasSlider.calibrate(temperature) + canvasSlider.marginsTemperaturesDrawing
                property int temperature: Utils.getTemperature(index, root.model)
                height: parent.height
                id: col
                Image {
                    source: Utils.getWeatherUrl(index, root.model)
                    width: canvasSlider.weatherIconWidth
                    height: width
                    anchors.horizontalCenter: col.horizontalCenter
                }
                Item {
                    height: 15 * ApplicationInfo.ratio
                    width: height
                }
                Image {
                    id: circle
                    source: ApplicationInfo.getImagePath("Circle.png")
                    width: canvasSlider.circleIconWidth
                    height: width
                    anchors.horizontalCenter: col.horizontalCenter
                }
                Item {
                    height: 15 * ApplicationInfo.ratio
                    width: height
                }
                TouchLabel {
                    text: Utils.getTempFormat(temperature)
                    pixelSize: 24
                    color: temperature > 0 ? ApplicationInfo.colors.doubleDarkGray : ApplicationInfo.colors.blue
                    anchors.horizontalCenter: col.horizontalCenter
                }
            }
        }
        ColumnLayout {
            id: column
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            TouchSlider {
                id: touchSlider
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.preferredWidth: (model.periodCount() - 1) * canvasSlider.drawingOffset + ApplicationInfo.sliderHandleWidth
                minimumValue: 0
                maximumValue: model.periodCount() - 1
            }
            RowLayout {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.preferredWidth: model.periodCount() * canvasSlider.drawingOffset + ApplicationInfo.sliderHandleWidth
                spacing: 0
                Repeater {
                    model: root.model.periodCount() + 1
                    TouchLabel {
                        pixelSize: 24
                        Layout.fillWidth: true
                        horizontalAlignment: Qt.AlignHCenter
                        Layout.alignment: Qt.AlignBaseline
                        text: (!!root.model && index !== root.model.periodCount()) ? Utils.getFromTime(index, root.model) : Utils.getToTime(index-1, root.model)
                    }
                }
            }
        }

    }
    Separator {
        Layout.fillHeight: true
        Layout.fillWidth: ApplicationInfo.isPortraitMode ? true : false
        Layout.minimumHeight: 10
    }
}
