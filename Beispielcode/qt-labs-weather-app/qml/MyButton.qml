//Button.qml
import QtQuick 2.0

Rectangle {
    width: 100
    height: 100
    color: "#000000"
    gradient: Gradient {
        GradientStop {
            position: 0
            color: "#ffffff"
        }

        GradientStop {
            position: 0.987
            color: "#000000"
        }

        GradientStop {
            position: 0.513
            color: "#ffffff"
        }
    }
}
