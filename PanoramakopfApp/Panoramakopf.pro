#-------------------------------------------------
#
# Project created by QtCreator 2014-05-24T17:50:04
#
#-------------------------------------------------

QT       += core gui androidextras widgets quick qml bluetooth

TARGET = Panoramakopf

TEMPLATE = app

SOURCES += src/main.cpp \
        src/mainwidget.cpp \
        src/communication.cpp \
        src/planning/panoramaplanung.cpp \
        src/bluetooth/androidrfcomm.cpp \
        src/settingsloader.cpp \
        src/time_delta.cpp


HEADERS  += src/mainwidget.h \
        src/communication.h \
        src/planning/panoramaplanung.h \
        src/protocoldefs.h \
        src/qtquickcontrolsapplication.h \
        src/bluetooth/androidrfcomm.h \
        src/settingsloader.h \
        src/osdefine.h \
        src/time_delta.h


#FORMS    +=

CONFIG += mobility qml_debug c++11

#MOBILITY =

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

OTHER_FILES += android/AndroidManifest.xml \
               qml/Homescreen.qml \
               qml/MyButton.qml \
               qml/MyCircle.qml \
               qml/TitleBar.qml \
               qml/MyToolBar.qml \
               qml/LoadingIndicator.qml \
               qml/ProgressIndicatorRectangle.qml \
               qml/DialogAddCamera.qml \
               qml/progressindicator.js


RESOURCES += \
    Resources.qrc
