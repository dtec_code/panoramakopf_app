#-------------------------------------------------
#
# Project created by QtCreator 2014-05-24T17:50:04
#
#-------------------------------------------------

TEMPLATE = app

QT       += core quick qml gui
#QT       += androidextras bluetooth
qtHaveModule(widgets): QT += widgets

TARGET = Panoramakopf

SOURCES += $$PWD/src/main.cpp \
        $$PWD/src/mainwidget.cpp \
        $$PWD/src/communication.cpp \
        $$PWD/src/planning/panoramaplanung.cpp \
        $$PWD/src/bluetooth/androidrfcomm.cpp \
        $$PWD/src/settingsloader.cpp \
        $$PWD/src/time_delta.cpp


HEADERS  += $$PWD/src/mainwidget.h \
        $$PWD/src/communication.h \
        $$PWD/src/planning/panoramaplanung.h \
        $$PWD/src/protocoldefs.h \
        $$PWD/src/qtquickcontrolsapplication.h \
        $$PWD/src/bluetooth/androidrfcomm.h \
        $$PWD/src/settingsloader.h \
        $$PWD/src/osdefine.h \
        $$PWD/src/time_delta.h


CONFIG += mobility qml_debug c++11 #debug


#QML_IMPORT_PATH = "./qml"

#OTHER_FILES += android/AndroidManifest.xml \
OTHER_FILES += qml/*.qml \
               qml/images/*.png \
               qml/*.js \
               qml/images/*.sci
#               qml/Homescreen.qml \
#               qml/MyButton.qml \
#               qml/MyCircle.qml \
#               qml/TitleBar.qml \
#               qml/MyToolBar.qml \
#               qml/LoadingIndicator.qml \
#               qml/ProgressIndicatorRectangle.qml \
#               qml/DialogAddCamera.qml \
#               qml/progressindicator.js


RESOURCES += Resources.qrc

#ios {
#   QMAKE_INFO_PLIST = Info.plist
#
#   iphonesimulator {
#      message("iphonesimulator")
#      DEFINES += iphonesimulator
#    }
#
#    iphoneos{
#      message("iphoneos")
#      DEFINES += iphoneos
#    }
#}
