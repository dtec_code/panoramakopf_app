import QtQuick 2.0

Item {
    id: titleBar

    BorderImage {
        source: "images/titlebar.sci"
        width: parent.width
        height: parent.height + 14
        y: -7
    }

    Item {
        id: container
        width: parent.width
        height: parent.height

        Text {
            id: categoryText

            font.family: "Helvetica"
            font.pixelSize: 35
            text: "Panoramaplaner"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter


            font.bold: true; color: "White"; style: Text.Raised; styleColor: "Black"
        }
    }

    transitions: Transition {
        NumberAnimation { properties: "x"; easing.type: Easing.InOutQuad }
    }
}
