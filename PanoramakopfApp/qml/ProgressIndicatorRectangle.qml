import QtQuick 2.2

Item {
    id: loadingIndicator
    width: parent.width

    function startAnimation() {
        loadingAnimation.start();
    }

    function stopAnimation() {
        loadingAnimation.restart();
        loadingAnimation.stop();
        effectRect.x = 0;
    }

    Rectangle {
        id: effectRect
        width: 6
        height: 6
        color: "steelblue"
        anchors.verticalCenter: parent.verticalCenter
        x: 0
        radius: width*0.5
    }

    SequentialAnimation {
        id: loadingAnimation
        alwaysRunToEnd: true
        loops: Animation.Infinite

        PropertyAction { target: effectRect; property: "visible"; value: true }
        PropertyAction { target: effectRect; property: "x"; value: 0 }
        PropertyAnimation { target: effectRect; property: "x"; to: loadingIndicator.width/3; duration: 400; }
        PropertyAnimation { target: effectRect; property: "x"; to: (loadingIndicator.width/3)*2; duration: 1000; }
        PropertyAnimation { target: effectRect; property: "x"; to: loadingIndicator.width; duration: 400; }
        PropertyAction { target: effectRect; property: "visible"; value: false }
        PauseAnimation { duration: 700 }
    }

}
