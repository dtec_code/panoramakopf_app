import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

Rectangle {
    id: screen2
    objectName: "screen2"
    anchors.fill: parent
    color: "blue";
    Image { source: "images/stripes.png"; fillMode: Image.Tile; anchors.fill: parent; opacity: 0.3 }




    SystemPalette { id: activePalette }

    /*Item {
        width: parent.width
        anchors { top: parent.top; bottom: toolBar.top }

        Image {
            id: background
            anchors.fill: parent
            source: "images/background.jpg"
            fillMode: Image.PreserveAspectCrop
        }
    }*/

    TitleBar {
        id: titleBar
        z: 5
        width: parent.width
        height: 40
        opacity: 0.9
    }

    ScrollView{

        anchors.top: titleBar.bottom
        anchors.topMargin: 15
        anchors.left: screen.left
        anchors.leftMargin: 20
        anchors.right: screen.right
        anchors.rightMargin: 15
        anchors.bottom: toolBar.top
        anchors.bottomMargin: 15


        GridLayout {
            id: gridLayout/*
            anchors.top: titleBar.bottom
            anchors.topMargin: 15
            anchors.left: screen.left
            anchors.leftMargin: 20
            anchors.bottom: toolBar.top
            anchors.bottomMargin: 15*/

            columns: 2
            columnSpacing: 15
            //columnSpacing: 2
            rowSpacing: 18
            //rows: 5


            Text {
                id: panoramaartText
                color: "white"
                objectName: "panoramaartText"
                //anchors.left: parent.left
                //anchors.leftMargin: 70
                text: qsTr("Panoramaart")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 20
            }




            ComboBox{
                id: panoramaartComboBox
                objectName: "panoramaartComboBox"
                width: 150
                model: [ "Kugel", "Zylinder"]
            }

            Text {
                id: blickwinkel
                color: "white"
                objectName: "blickwinkel"
                text: qsTr("Blickwinkel")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 20
            }

            ComboBox{
                id: blickwinkelComboBox
                objectName: "panoramaartComboBox"
                width: 150
                model: [ "90", "180", "270", "360"]
            }

            Text {
                id: ueberlappungsgradText
                color: "white"
                objectName: "ueberlappungsgradText"
                text: qsTr("Überlappungsgrad")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 20
            }

            Rectangle{
                // Damit die Breite des TextField eingestellt werden kann.
                width: 158
                // Hoehe ist niedrig, damit nur das TextField sichtbar ist.
                height: 40
                border.width: 0
                radius: 5
                //border.color: "#4b4b4b"
                opacity: 1

                TextField {
                    id: ueberlappungsgrad
                    textColor: "black"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    //color: "black"
                    font.pointSize: 20
                    placeholderText: qsTr("z. B. 0.3")
                    width: parent.width
                    inputMethodHints: Qt.ImhDigitsOnly
                    inputMask: "0.0"



                }
            }

            Text {
                id: cameraText
                color: "white"
                objectName: "cameraText"
                text: qsTr("Kamera")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 20
            }

            ComboBox{
                id: camera
                objectName: "cameraComboBox"
                width: 150
                model: [ "Canon EOS7D", "Sony bla", "Nikon bla", "Olympus bla", "Panasonic bla"]
            }





            Text {
                id: objectivText
                color: "white"
                objectName: "objectivText"
                text: qsTr("Objektiv")
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 20
            }

            ComboBox{
                id: objectiv
                objectName: "objectivComboBox"
                width: 150
                model: [ "Objektiv1", "Objektiv2", "Objektiv3", "Objektiv4", "Objektiv5"]
            }





        }

    }



    MyToolBar{
        id: toolBar
        objectName: "toolBar"
        //x: 2;
        //y: 440
        //z: 5
        height: toolBar.button1Height+20; antialiasing: false; anchors.bottom: parent.bottom//; anchors.bottomMargin: 0
        width: parent.width; opacity: 0.9
        leftButtonLabel: "blabla"; rightButtonLabel: "Settings"
        //onButton1Clicked: restModel.reload()
        //onButton2Clicked: if (screen.inListView == true) screen.inListView = false; else screen.inListView = true

        Button{
            id: command
            objectName: "command"
            text: "Starte"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
    }

    }



    /*
    Circle {
        id: statusCircle
        anchors.bottomMargin: 100
        objectName: "statusCircle"
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: -statusCircle.cirlceWidth/2
        anchors.bottom: toolBar.top
    }
    */

    /*
    MyCanvas {
        width: 300
        height: 300
        /*LoadCircle {
            anchors.centerIn: parent
            Component.onCompleted: start();
        }

        x: 80
        y: 80
    }
*/
    /*Rectangle {
        id: toolBar
        width: parent.width; height: buttonLabel.height
        color: activePalette.window
        anchors.bottom: screen.bottom

        Button {
            id: buttonLabel
            objectName: "buttonLabel"
            anchors { left: parent.left; verticalCenter: parent.verticalCenter }
            text: "Verbinden"
        }

        Text {
            id: score
            anchors { right: parent.right; verticalCenter: parent.verticalCenter }
            text: "Score: Who knows?"
        }
    }*/
}
