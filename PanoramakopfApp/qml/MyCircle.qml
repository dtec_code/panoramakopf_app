import QtQuick 2.0

Item{

    id: container
    signal clicked
    property alias diameter: cirlce.width

    Rectangle {
        id: cirlce
        width: 50
        height: 50
        color: "transparent"
        border.color: "white"
        border.width: 3
        radius: width*0.8
        opacity: 0.9

        Rectangle {
            id: cirlce2
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: cirlce.width/2
            height: cirlce.width/2
            color: "red"
            border.color: "transparent"
            border.width: 1
            radius: width*0.8
            opacity: 0.9

        }

        Rectangle {
            id: cirlcePressed
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: cirlce.width/2
            height: cirlce.width/2
            color: "green"
            border.color: "transparent"
            border.width: 1
            radius: width*0.8
            opacity: 0

        }

    }

    MouseArea {
        id: mouseRegion
        anchors.fill: cirlce
        onClicked: { container.clicked(); }
    }

    states: [
        State {
            name: "Pressed"
            when: mouseRegion.pressed === true
            PropertyChanges {
                target: cirlcePressed
                opacity: 1
            }
        }
    ]


}
