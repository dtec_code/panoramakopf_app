import QtQuick 2.2
import "progressindicator.js" as Indicator

Item {
    id: progressBarComponent
    height: 10

    property bool running: false;
    property int numberOfElements: 0;


    function start() {
        console.log("Animation started.");

        indicatorCreationTimer.restart();
        progressBarComponent.running = true;
    }

    function stop() {
        console.log("Animation stopped.");

        progressBarComponent.running = false;
        Indicator.stopIndicators();
    }

    Timer {
        id: indicatorCreationTimer;
        running: false
        repeat: true;
        interval: 200;
        onTriggered: {
            if (!Indicator.allStarted) {
                Indicator.startNextIndicator();
            } else {
                indicatorCreationTimer.stop();
            }
        }
    }

}
