import QtQuick 2.2

Item {
    id: toolbar
    property alias rightButtonObj: buttonRight
    property alias leftButtonObj: buttonLeft
    property alias leftButtonLabel: buttonLeft.text
    property alias button1Height: buttonLeft.height
    property alias rightButtonLabel: buttonRight.text
    property alias button2Height: buttonRight.height
    property int itemHeight: buttonLeft.height+30
    property int buttonWidth: 170

    BorderImage {
        source: "images/titlebar.sci"; width: parent.width; height: itemHeight;
    }


    MyButton {
        id: buttonLeft
        objectName: "buttonLeft"
        anchors.left: parent.left; anchors.leftMargin: 10; anchors.top: parent.top; anchors.topMargin: 12
        width: buttonWidth; height: 50
    }

    MyButton {
        id: buttonRight
        anchors.right: parent.right; anchors.rightMargin: 10; anchors.top: parent.top; anchors.topMargin: 12
        width: buttonWidth; height: 50
    }


}
