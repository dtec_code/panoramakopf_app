import QtQuick 2.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Controls 1.2

/*Item {
    property alias myExclusiveGroup: radioButton.exclusiveGroup
    property string imageSource: radioButtonItemImage.source
    property int imageSourceSizeWidth: radioButtonItemImage.sourceSize.width // 200
    property int imageSourceSizeHeight: radioButtonItemImage.sourceSize.height //100
    property int imageWidth: radioButtonItemImage.width // 200
    property int imageHeight: radioButtonItemImage.height //100
    //property alias textFieldInX: textField.anchors.leftMargin // radioButtonItemImage.width/4 + 22
    //property alias radioButtonImageRef: radioButtonItemImage
  */
    RadioButton {
        id: radioButton
        text: " "
        //exclusiveGroup: tabPositionGroup
        style: RadioButtonStyle {
            indicator: Rectangle {
                implicitWidth: 30
                implicitHeight: 30
                radius: 18
                border.color: control.activeFocus ? "darkblue" : "gray"
                border.width: 1
                Image {
                    id: radioButtonItemImage
                    anchors.left: parent.right
                    anchors.leftMargin: 5
                    //width: radioButtonItemImage.width; height: radioButtonItemImage.height
                    antialiasing: true
                    z: 5
                    //sourceSize.height: radioButtonItemImage.height
                    //sourceSize.width: radioButtonItemImage.width
                    //fillMode: Image.PreserveAspectFit
                    smooth: true
                    //source: "images/rotate_x_1.png"
                    opacity: 0.95
                }
                TextField {
                    id: textField
                    anchors.bottom: radioButtonItemImage.bottom
                    anchors.bottomMargin: 0
                    anchors.left: radioButtonItemImage.left
                    anchors.leftMargin: radioButtonItemImage.width/4 + 22
                    placeholderText: qsTr("in °")
                    validator: IntValidator {bottom: 0; top: 360}
                    inputMethodHints: Qt.ImhDigitsOnly

                    style: TextFieldStyle {
                        textColor: "black"
                        background: Rectangle {
                            radius: 5
                            implicitWidth: 70
                            implicitHeight: 40
                            border.color: "#333"
                            border.width: 1
                        }
                    }
                }
                Rectangle {
                    anchors.fill: parent
                    visible: control.checked
                    color: "#555"
                    radius: 10
                    anchors.margins: 7
                }
            }
        }
    }
//}
