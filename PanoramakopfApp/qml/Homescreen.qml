import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.1



ApplicationWindow {

    id: mainWindow
    visible: true
    objectName: "mainWindow"
    color: "#343434"
    Image { source: "images/stripes.png"; fillMode: Image.Tile; anchors.fill: parent; opacity: 0.3 }

    width: 800
    height: 1024

    TitleBar {
        id: titleBar
        z: 5
        width: parent.width
        height: 50
        opacity: 0.9
    }


    MouseArea {
        id: recognizedMouseArea
        width: recognizedCamera.width
        height: recognizedCamera.height
        anchors.verticalCenter: titleBar.verticalCenter
        anchors.left: titleBar.left
        anchors.leftMargin: 20
        z: 5
        Image {
            id: recognizedCamera
            objectName: "recognizedCamera"
            width: 32; height: 24
            antialiasing: true
            sourceSize.height: 32
            sourceSize.width: 24
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "images/camera.png"
            opacity: 1
        }
        onClicked: {
            cameraInformationDialog.open()
        }
    }

    MouseArea {
        width: addCameraSensor.width
        height: addCameraSensor.height
        anchors.verticalCenter: titleBar.verticalCenter
        anchors.left: recognizedMouseArea.right
        anchors.leftMargin: 15
        z: 5
        Image {
            id: addCameraSensor
            objectName: "addCameraSensor"
            width: 32; height: 24
            antialiasing: true
            sourceSize.height: 32
            sourceSize.width: 24
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "images/addCameraSensor.png"
            opacity: 1
        }
        onClicked:
        {
            //Qt.createComponent("DialogAddCamera.qml").component.createObject(mainWindow, {});
            dialogAddCamera.visible = true

        }
    }

    DialogAddCamera{
       id: dialogAddCamera
       z: 5
       visible: false
    }




    Image {
        id: connectionRed
        objectName: "connectionRed"
        anchors.verticalCenter: titleBar.verticalCenter
        anchors.right: titleBar.right
        anchors.rightMargin: 20
        width: 32; height: 32
        antialiasing: true
        z: 5
        sourceSize.height: 32
        sourceSize.width: 32
        fillMode: Image.PreserveAspectFit
        smooth: true
        source: "images/connection_red.png"
        opacity: 0.9
    }

    Image {
        id: connectionGreen
        objectName: "connectionGreen"
        anchors.verticalCenter: titleBar.verticalCenter
        anchors.right: titleBar.right
        anchors.rightMargin: 20
        width: 32; height: 32
        antialiasing: true
        z: 5
        sourceSize.height: 32
        sourceSize.width: 32
        fillMode: Image.PreserveAspectFit
        smooth: true
        source: "images/connection_green.png"
        opacity: 0
    }

    Image {
        id: connectionRed_Green
        objectName: "connectionRed_Green"
        anchors.verticalCenter: titleBar.verticalCenter
        anchors.right: titleBar.right
        anchors.rightMargin: 20
        width: 32; height: 32
        antialiasing: true
        z: 5
        sourceSize.height: 32
        sourceSize.width: 32
        fillMode: Image.PreserveAspectFit
        smooth: true
        source: "images/connection_red_green.png"
        opacity: 0
    }

    Image {
        id: connectionGreen_Red
        objectName: "connectionGreen_Red"
        anchors.verticalCenter: titleBar.verticalCenter
        anchors.right: titleBar.right
        anchors.rightMargin: 20
        width: 32; height: 32
        antialiasing: true
        z: 5
        sourceSize.height: 32
        sourceSize.width: 32
        fillMode: Image.PreserveAspectFit
        smooth: true
        source: "images/connection_green_red.png"
        opacity: 0
    }

    LoadingIndicator {
        id: loadingIndicator
        objectName: "loadingIndicator"
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.top: titleBar.bottom
        anchors.topMargin: 1
        z:5

        function activateLoadingIndicator(isActive)
        {
            if(isActive)
            {
                   loadingIndicator.start();
            }
            else
            {
                loadingIndicator.stop();
            }
        }
    }


/*
    ProgressBar {
        id: progressBar
        objectName: "progressBar"
        height: 8
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.top: titleBar.bottom
        anchors.topMargin: 1
        value: 0
        opacity: 0
        style: ProgressBarStyle{
            background: Rectangle{
                radius: 2
                border.color: "#333"
                border.width: 1

            }
            progress: Rectangle {
                color: "lightsteelblue"
                border.color: "steelblue"
            }
        }
        onValueChanged: {
            if(value < 0.99)
            {
                progressBar.opacity = 1
            }
            else
            {
                progressBar.opacity = 0
            }
        }

    }
*/

    ScrollView{
        id: mainView
        anchors.top: titleBar.bottom
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: toolBar.top
        anchors.bottomMargin: 15

        GridLayout {
            id: gridLayout
            columns: 2
            columnSpacing: 40
            rowSpacing: 40

            // 1. line
            Text {
                color: "white"
                text: qsTr("Panoramaart")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            ComboBox{
                id: panoramaKindComboBox
                objectName: "panoramaKindComboBox"
                style: ComboBoxStyle {
                    background: Rectangle {
                        radius: 5
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "#333"
                        border.width: 1
                    }
                }

                model: ListModel {
                    ListElement { text: "Kugel"}
                    ListElement { text: "Zylinder"}
                }

                // if "Kugel" is not selected, then let the user input his own values
                onCurrentTextChanged: {
                    if (panoramaKindComboBox.currentText === "Kugel"){
                        horizSpinBox.value = 180
                        horizSpinBox.enabled = false
                        vertSpinBox.value = 90
                        vertSpinBox.enabled = false
                    }
                    else{
                        horizSpinBox.enabled = true
                        vertSpinBox.enabled = true
                    }
                }
            }

            // 2. line
            Text {
                color: "white"
                text: qsTr("Horizontaler\nBildwinkel")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            SpinBox {
                id: horizSpinBox
                objectName: "horizSpinBox"
                style: SpinBoxStyle{
                    background: Rectangle {
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "grey"
                        radius: 5
                    }
                }
                horizontalAlignment: Text.AlignRight
                minimumValue: 0
                maximumValue: 180

            }

            // 3. line
            Text {
                color: "white"
                text: qsTr("Vertikaler\nBildwinkel")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            SpinBox {
                id: vertSpinBox
                objectName: "vertSpinBox"
                style: SpinBoxStyle{
                    background: Rectangle {
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "grey"
                        radius: 5
                    }
                }
                minimumValue: 0
                maximumValue: 90

            }

            // 4. line
            Text {
                color: "white"
                text: qsTr("Überlappungs-\ngrad")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            ComboBox{
                id: overlayDegreeComboBox
                objectName: "overlayDegreeComboBox"
                style: ComboBoxStyle {
                    background: Rectangle {
                        radius: 5
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "#333"
                        border.width: 1
                    }
                }
                model: [ "20 %", "25 %", "30 %", "35 %", "40 %"]
                // set default-value (here: 30 %)
                currentIndex: 2

            }

            // 5. line
            Text {
                color: "white"
                text: qsTr("Kamera")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            ComboBox{
                id: cameraComboBox
                objectName: "cameraComboBox"
                style: ComboBoxStyle {
                    background: Rectangle {
                        radius: 5
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "#333"
                        border.width: 1
                    }
                }
                model: ListModel {}

                // if "Benutzerdefiniert" is not selected, then let the user input his own values, else set preconfigured values
                onCurrentTextChanged: {
                    if (cameraComboBox.currentText === "Benutzerdefiniert"){
                        sensorComboBox.enabled = true
                    }
                    else // selection of the appropriate sensor
                    {
                        sensorComboBox.enabled = false
                        // contains the array[i] the selected camera?!

                        var key = getKey(cameraInformationMap, cameraComboBox.currentText);
                        if(key !== null){
                            var sensorKey = cameraInformationMap[key].split(";")[1];
                            var sensorName = sensorInformationMap[sensorKey].split(";")[0];
                            console.log("Sensorname: " + sensorName);
                            var sensorIndex = sensorComboBox.find(sensorName);
                            console.log("SensorIndex: " + sensorIndex);
                            sensorComboBox.currentIndex = sensorIndex;
                        }
                    }
                }

                property var cameraInformationMap : [];
                property var sensorInformationMap : []; // key: z. b. Camera0 value: Sony bla;Sensor0

                function getKey(map, value){
                    for(var key in map){
                        if(map[key].search(value) !== -1){
                            return key;
                        }
                    }
                    return null;
                }

                function append(newElement) {
                    cameraComboBox.model.append({"name":newElement})
                }

                function addCameraInformation(key, element)
                {
                    cameraInformationMap[key] = element;
                    console.log(cameraInformationMap[key])
                }

                function addSensorInformation(key, element)
                {
                    sensorInformationMap[key] = element;
                    console.log(sensorInformationMap[key])
                }

                function selectRecognizedCamera(recognizedCamera)
                {
                    console.log(recognizedCamera);
                    for(var key in cameraInformationMap)
                    {
                        if(cameraInformationMap[key].search(recognizedCamera) !== -1)
                        {
                            var cameraIndex = cameraComboBox.find(recognizedCamera);
                            console.log("index: " + cameraIndex);
                            cameraComboBox.currentIndex = cameraIndex;
                            break;
                        }
                    }

                }
            }


            // 6. line
            Text {
                color: "white"
                text: qsTr("Sensor")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            ComboBox{
                id: sensorComboBox
                objectName: "sensorComboBox"
                style: ComboBoxStyle {
                    background: Rectangle {
                        radius: 5
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "#333"
                        border.width: 1
                    }
                }
                model: ListModel {}

                function append(newElement) {
                    sensorComboBox.model.append({"name":newElement})
                }
            }

            // 7. line
            Text {
                color: "white"
                text: qsTr("Brennweite")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            TextField {
                id: focalLength
                objectName: "focalLength"
                placeholderText: qsTr("in mm")
                validator: IntValidator {bottom: 0}
                inputMethodHints: Qt.ImhFormattedNumbersOnly

                style: TextFieldStyle {
                    textColor: "black"
                    background: Rectangle {
                        radius: 5
                        implicitWidth: 180
                        implicitHeight: 50
                        border.color: "#333"
                        border.width: 1
                    }
                }
            }

            // 8. line
            Text {
                color: "white"
                text: qsTr("Horizontaler\nDrehwinkel")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            Text {
                objectName: "horizontalRotationAngle"
                color: "white"
                text: qsTr("-")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            // 9. line
            Text {
                color: "white"
                text: qsTr("Vertikaler\nDrehwinkel")
                font.family: "Helvetica"
                font.pointSize: 20
            }

            Text {
                objectName: "verticalRotationAngle"
                color: "white"
                text: qsTr("-")
                font.family: "Helvetica"
                font.pointSize: 20
            }
        }

    }



    ScrollView{
        id: manualView
        visible: false;
        anchors.top: titleBar.bottom
        anchors.topMargin: 15
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.bottom: toolBar.top
        anchors.bottomMargin: 15


        GroupBox {

            GridLayout {
                columns: 2
                ExclusiveGroup {
                    id: tabPositionGroup
                    objectName: "tabPositionGroup"
                    // which radioButton is active?
                    // 0    1
                    // 2    3
                    // 4    5
                    // 6    7
                    property int activeIndex: 0;
                }

                RadioButton {
                    text: "                            "
                    checked: true
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Image {
                                id: rotateLeftImage
                                anchors.left: parent.right; anchors.leftMargin: 5
                                width: 200; height: 100
                                antialiasing: true
                                z: 5
                                sourceSize.height: 100; sourceSize.width: 200
                                //fillMode: Image.PreserveAspectFit
                                smooth: true
                                source: "images/rotate_x_1.png"
                                opacity: 0.95
                            }
                            TextField {
                                objectName: "rotateX1TextField"
                                font.family: "Helvetica"
                                font.pointSize: 14
                                anchors.bottom: rotateLeftImage.bottom
                                anchors.bottomMargin: 0
                                anchors.left: rotateLeftImage.left
                                anchors.leftMargin: rotateLeftImage.width/4 + 22
                                placeholderText: qsTr("in °")
                                validator: IntValidator {bottom: 0; top: 360}
                                inputMethodHints: Qt.ImhDigitsOnly

                                style: TextFieldStyle {
                                    textColor: "black"
                                    background: Rectangle {
                                        radius: 5
                                        implicitWidth: 70; implicitHeight: 40
                                        border.color: "#333"
                                        border.width: 1
                                    }
                                }
                            }
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 0;}
                }

                RadioButton {
                    // Damit der Rand der Gruppe weiter rechts ist und dient zum scrollen, falls Bild zu schmall
                    text: "                            "
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Image {
                                id: rotateRightImage
                                anchors.left: parent.right; anchors.leftMargin: 5
                                width: 200; height: 100
                                antialiasing: true
                                z: 5
                                sourceSize.height: 100; sourceSize.width: 200
                                //fillMode: Image.PreserveAspectFit
                                smooth: true
                                source: "images/rotate_x_2.png"
                                opacity: 0.95
                            }
                            TextField {
                                objectName: "rotateX2TextField"
                                font.family: "Helvetica"
                                font.pointSize: 12
                                anchors.bottom: rotateRightImage.bottom
                                anchors.bottomMargin: 0
                                anchors.left: rotateRightImage.left
                                anchors.leftMargin: rotateRightImage.width/4 + 13
                                placeholderText: qsTr("in °")
                                validator: IntValidator {bottom: 0; top: 360}
                                inputMethodHints: Qt.ImhDigitsOnly

                                style: TextFieldStyle {
                                    textColor: "black"
                                    background: Rectangle {
                                        radius: 5
                                        implicitWidth: 70; implicitHeight: 40
                                        border.color: "#333"
                                        border.width: 1
                                    }
                                }
                            }
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 1;}
                }

                RadioButton {
                    text: "                            "
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Image {
                                id: rotateDownImage
                                anchors.left: parent.right; anchors.leftMargin: 5
                                width: 95; height: 170
                                antialiasing: true
                                z: 5
                                sourceSize.height: 170; sourceSize.width: 95
                                //fillMode: Image.PreserveAspectFit
                                smooth: true
                                source: "images/rotate_z_2.png"
                                opacity: 0.95
                            }
                            TextField {
                                objectName: "rotateZ2TextField"
                                font.family: "Helvetica"
                                font.pointSize: 12
                                anchors.bottom: rotateDownImage.bottom
                                anchors.bottomMargin: 70
                                anchors.left: rotateDownImage.left
                                anchors.leftMargin: rotateDownImage.width/4 + 28
                                placeholderText: qsTr("in °")
                                validator: IntValidator {bottom: 0; top: 180}
                                inputMethodHints: Qt.ImhDigitsOnly

                                style: TextFieldStyle {
                                    textColor: "black"
                                    background: Rectangle {
                                        radius: 5
                                        implicitWidth: 70; implicitHeight: 40
                                        border.color: "#333"
                                        border.width: 1
                                    }
                                }
                            }
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 2;}
                }

                RadioButton {
                    // Abstand zu den oberen Elementen
                    text: "\n\n\n\n\n\n\n\n"
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Image {
                                id: rotateUpImage
                                anchors.left: parent.right; anchors.leftMargin: 5
                                width: 95; height: 170
                                antialiasing: true
                                z: 5
                                sourceSize.height: 170; sourceSize.width: 95
                                //fillMode: Image.PreserveAspectFit
                                smooth: true
                                source: "images/rotate_z_1.png"
                                opacity: 0.95
                            }
                            TextField {
                                objectName: "rotateZ1TextField"
                                font.family: "Helvetica"
                                font.pointSize: 12
                                anchors.bottom: rotateUpImage.bottom
                                anchors.bottomMargin: 60
                                anchors.left: rotateUpImage.left
                                anchors.leftMargin: rotateUpImage.width/4 + 30
                                placeholderText: qsTr("in °")
                                validator: IntValidator {bottom: 0; top: 180}
                                inputMethodHints: Qt.ImhDigitsOnly

                                style: TextFieldStyle {
                                    textColor: "black"
                                    background: Rectangle {
                                        radius: 5
                                        implicitWidth: 70; implicitHeight: 40
                                        border.color: "#333"
                                        border.width: 1
                                    }
                                }
                            }
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 3;}
                }

                RadioButton {
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                            Label {
                                anchors.left: parent.right
                                anchors.leftMargin: 17
                                text: "Ausgangsposition"
                                font.family: "Helvetica"
                                font.pixelSize: 24
                                color: "white"
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 4;}
                }

                RadioButton {
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                            Label {
                                anchors.left: parent.right
                                anchors.leftMargin: 17
                                text: "Schnappschuss"
                                font.family: "Helvetica"
                                font.pixelSize: 24
                                color: "white"
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 5;}
                }

                RadioButton {
                    // Abstand zu den oberen Elementen
                    text: "\n\n\n"
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                            Label {
                                anchors.left: parent.right
                                anchors.leftMargin: 17
                                text: "Starte\nKalibrierung"
                                font.family: "Helvetica"
                                font.pixelSize: 24
                                color: "white"
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 6;}
                }

                RadioButton {
                    // Abstand zu den oberen Elementen
                    text: "\n\n\n"
                    exclusiveGroup: tabPositionGroup
                    style: RadioButtonStyle {
                        indicator: Rectangle {
                            implicitWidth: 30; implicitHeight: 30
                            radius: 18
                            border.color: control.activeFocus ? "darkblue" : "gray"
                            border.width: 1
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#555"
                                radius: 10
                                anchors.margins: 7
                            }
                            Label {
                                anchors.left: parent.right
                                anchors.leftMargin: 17
                                text: "Übernehme\nKalibrierung"
                                font.family: "Helvetica"
                                font.pixelSize: 24
                                color: "white"
                            }
                        }
                    }
                    onCheckedChanged: if(checked){tabPositionGroup.activeIndex = 7;}
                }

            }
        }

    }



    MyToolBar{
        id: toolBar
        objectName: "toolBar"
        height: toolBar.button1Height+20; antialiasing: false; anchors.bottom: parent.bottom
        width: parent.width; opacity: 0.9
        leftButtonLabel: "Verbinden"; rightButtonLabel: "Manuell"

        // controlls which objects have to be seen. Switch status.
        rightButtonObj{
            onClicked: {
                if(mainView.visible){
                    mainView.visible = false
                    manualView.visible = true
                    rightButtonLabel = "Programm"
                    leftButtonObj.visible = false
                    startCommand.visible = false
                    startSingleCommand.visible = true
                }
                else{
                    mainView.visible = true
                    manualView.visible = false
                    rightButtonLabel = "Manuell"
                    leftButtonObj.visible = true
                    startCommand.visible = true
                    startSingleCommand.visible = false
                }
            }
        }





        MyCircle{
            id: startCommand
            objectName: "startCommand"
            x: parent.width/2 - diameter/2
            y: parent.height/2 - diameter/2 + 3
        }

        MyCircle{
            id: startSingleCommand
            objectName: "startSingleCommand"
            x: parent.width/2 - diameter/2
            y: parent.height/2 - diameter/2 + 3
            visible: false
        }
        /*
        Button{
            id: command
            objectName: "command"
            text: "Starte"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

              Loader{
                id:ld;
                anchors.fill: screen;
            }

            onClicked: {
                        ld.source="qrc:///qml///ButtonPage.qml"
                    }


        }
*/
    }

    MessageDialog {
        id: messageDialog
        objectName: "messageDialog"
        title: "Aufnahme nicht möglich"
        icon: StandardIcon.Warning
        text: "Bluetoothverbindung nicht vorhanden."
        Component.onCompleted: visible = false
    }


    MessageDialog {
        id: cameraInformationDialog
        objectName: "cameraInformationDialog"
        title: "Kamerainformationen"
        icon: StandardIcon.Information
        text: "Derzeit ist keine Kamera über USB angeschlossen."
        Component.onCompleted: visible = false
    }

}
