import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1

// Use an item as container to group both the overlay and the dialog
// I do this because the overlay itself has an opacity set, and otherwise
// also the dialog would have been semi-transparent.
// I use an Item instead of an Rectangle. Always use an 'Item' if it does not
// display stuff itself, this is better performance wise.
Item {
    id: dialogComponent
    anchors.fill: parent

    // Add a simple animation to fade in the popup
    // let the opacity go from 0 to 1 in 400ms
    PropertyAnimation { target: dialogComponent; property: "opacity";
                                  duration: 400; from: 0; to: 1;
                                  easing.type: Easing.InOutQuad ; running: true }

    // This rectange is the a overlay to partially show the parent through it
    // and clicking outside of the 'dialog' popup will do 'nothing'
    Rectangle {
        anchors.fill: parent
        id: overlay
        color: "#000000"
        opacity: 0.6
        // add a mouse area so that clicks outside
        // the dialog window will not do anything
        MouseArea {
            anchors.fill: parent
        }
    }

    // This rectangle is the actual popup
    Rectangle {
        id: dialogWindow
        width: 500
        height: 420
        radius: 10
        anchors.centerIn: parent
        color: "#343434"
        Image { source: "images/stripes.png"; fillMode: Image.Tile; anchors.fill: parent; opacity: 0.3 }

        Text {
            id: title
            //anchors.centerIn: parent
            anchors.horizontalCenter: parent .horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 6
            text: "Kamera/Sensor hinzufügen"
            font.pixelSize: 30
            font.family: "Helvetica"
            color: "white"
        }


        Text {
            id: kameraText
            text: "Kamera:"
            font.pixelSize: 25
            color: "white"
            anchors.left: parent.left
            anchors.leftMargin: 34
            anchors.top: title.bottom
            anchors.topMargin: 40
            font.family: "Helvetica"
        }

        TextField {
            id: kameraNameTextField
            font.pixelSize: 22
            objectName: "kameraNameTextField"
            anchors.verticalCenter: kameraText.verticalCenter
            anchors.left: kameraText.right
            anchors.leftMargin: 100
            style: TextFieldStyle {
                textColor: "black"
                background: Rectangle {
                    radius: 5
                    implicitWidth: 180
                    implicitHeight: 50
                    border.color: "#333"
                    border.width: 1
                }
            }
        }


        Text {
            id: sensorNameText
            text: "Sensorname:"
            font.pixelSize: 25
            anchors.left: kameraText.left
            anchors.leftMargin: 0
            color: "white"
            font.family: "Helvetica"
            anchors.top: kameraText.bottom
            anchors.topMargin: 30
        }

        TextField {
            id: sensorNameTextField
            font.pixelSize: 22
            objectName: "sensorNameTextField"
            anchors.horizontalCenter: kameraNameTextField.horizontalCenter
            anchors.verticalCenter: sensorNameText.verticalCenter
            style: TextFieldStyle {
                textColor: "black"
                background: Rectangle {
                    radius: 5
                    implicitWidth: 180
                    implicitHeight: 50
                    border.color: "#333"
                    border.width: 1
                }
            }
        }

        Text {
            id: sensorBreiteText
            text: "Sensorbreite:"
            font.pixelSize: 25
            anchors.left: kameraText.left
            anchors.leftMargin: 0
            color: "white"
            font.family: "Helvetica"
            anchors.top: sensorNameText.bottom
            anchors.topMargin: 30
        }

        TextField {
            id: sensorBreiteTextField
            font.pixelSize: 22
            anchors.horizontalCenter: kameraNameTextField.horizontalCenter
            anchors.verticalCenter: sensorBreiteText.verticalCenter
            objectName: "sensorBreiteTextField"
            style: TextFieldStyle {
                textColor: "black"
                background: Rectangle {
                    radius: 5
                    implicitWidth: 180
                    implicitHeight: 50
                    border.color: "#333"
                    border.width: 1
                }
            }
        }

        Text {
            id: sensorHoeheText
            text: "Sensorhöhe:"
            font.pixelSize: 25
            anchors.left: kameraText.left
            anchors.leftMargin: 0
            color: "white"
            font.family: "Helvetica"
            anchors.top: sensorBreiteText.bottom
            anchors.topMargin: 30
        }

        TextField {
            id: sensorHoeheTextField
            font.pixelSize: 22
            anchors.horizontalCenter: sensorBreiteTextField.horizontalCenter
            anchors.verticalCenter: sensorHoeheText.verticalCenter
            objectName: "sensorHoeheTextField"
            style: TextFieldStyle {
                textColor: "black"
                background: Rectangle {
                    radius: 5
                    implicitWidth: 180
                    implicitHeight: 50
                    border.color: "#333"
                    border.width: 1
                }
            }
        }

        MyButton {
            id: buttonUebernehmen
            objectName: "buttonUebernehmen"
            text: "Übernehmen"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 7
            anchors.left: parent.left; anchors.leftMargin: 10;
            width: 200; height: 50; onClicked: {
                // destroy object is needed when you dynamically create it
                //dialogComponent.destroy()
                dialogComponent.visible = false;
            }
        }

        MyButton {
            id: buttonAbbrechen
            width: 200
            objectName: "buttonAbbrechen"
            text: "Abbrechen"
            anchors.verticalCenter: buttonUebernehmen.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 10
            height: 50
            onClicked: {
                // destroy object is needed when you dynamically create it
                //dialogComponent.destroy()
                dialogComponent.visible = false;
            }
        }
    }
}
