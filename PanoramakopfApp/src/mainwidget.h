#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#ifdef ANDROID
   #include "bluetooth/androidrfcomm.h"
#endif
#include <QWidget>
#include <QDebug>
#include <QTimer>
#include "planning/panoramaplanung.h"
#include "communication.h"
#include "stdlib.h"
#include <QObject>
#include <QQmlApplicationEngine>
#include <QThread>
#include "settingsloader.h"
#include <QThread>
#include <QtCore>

class MainWidget : public QWidget
{
    Q_OBJECT

public:
    // QML
    explicit MainWidget(QQmlApplicationEngine *engine, SettingsLoader *settingsLoader, QWidget *parent = 0);
    // C++
    //explicit MainWidget(QWidget *parent = 0);


private slots:
    void periodialCheckStatusRun();
    void connectionStatusBlink();
    void clickLeftButton();
    void addCameraData_SensorData();
    void quit();
    void beginRecording();
    void beginSingleCommand();
    void checkProgressStatus();




private:
    QThread *workerThread;
    SettingsLoader *settingsLoader;
#ifdef ANDROID
    AndroidRfComm rfcomm;
#endif
    QObject *rootObject;
    QTimer *timer;
    QTimer *timerConnectToPanoramaHead;
    QTimer *checkProgress;
    QObject *connectionButton;
    QObject *addCamera_SensorButton;
    QString getStringFromUnsignedChar( unsigned char *str );
    Communication *communication;
    Panoramaplanung *panoramaplanung;

    enum ConnectionStatus {GREEN,RED,GREEN_RED,RED_GREEN};
    bool blinkStatus = true;
    bool recognizedAndChooseCamera = true;

    void setCameraInformationTextInDialog(QString cameraInfo);
    void disconnectingProcedure();
    void activateLoadingIndicator(bool active);
    bool selectRecognizedCamera(QString cameraInfo);


public:
    void switchConnectionStatus(ConnectionStatus status);
    void setProgressValue(float value);
    void setOpacityOfProgressBar(float value);
    void showMessageDialog(int error);
};

#endif // MAINWIDGET_H
