
/**
* @file
* @author Kirill Wetoschkin
* @version 1.0
*/

#include "communication.h"

unsigned char Communication::buffer[1+8+240+1];
unsigned char Communication::bufferlength;

Communication::Communication(QObject* parent) : QObject(parent)
{
    pano_process_data.clear();

#ifdef ANDROID
    // Check if Bluetooth is available on this device
    if (localDevice.isValid()) {

        // Turn Bluetooth on
        localDevice.powerOn();
    }
#endif

    // Initialisierung, um Fehler beim Verbinden zu vermeiden
    deltaTime1 = QTime::currentTime();
    qDebug() << "Constructor - DeltaTime: " + deltaTime1.toString();
    deltaTime1 = deltaTime1.addSecs(TIMEOUT * (-2));
    qDebug() << "Constructor - DeltaTime - Subtract: " + deltaTime1.toString();

#ifdef ANDROID
    rfcomm = new AndroidRfComm();
#endif

}


void Communication::connectToDevice()
{
#ifdef ANDROID
    if(!rfcomm->isConnected() && rfcomm->isEnabled()){
        rfcomm->connect("Panoramakopf");
    }
    if(rfcomm->isConnected() && rfcomm->isEnabled())
    {
        connected = true;
        deltaTime1 = QTime::currentTime();
    }
    else
    {
        connected = false;
    }
#endif

}

bool Communication::disconnectFromDevice()
{
#ifdef ANDROID
    if(rfcomm->isConnected() && rfcomm->isEnabled())
    {
        // deactivate Bluetooth
        //localDevice.setHostMode(QBluetoothLocalDevice::HostPoweredOff);

        qDebug() << "Vor disconnect()";
        rfcomm->disconnect();
        qDebug() << "Nach disconnect()";

        return true;
    }
    else
    {
        return false;
    }
#endif

}

_pano_process_data Communication::getPanoProcessData()
{
    return pano_process_data;
}

void Communication::connectionBuffer()
{
    if(connected)
    {
        QString str,rcv;
        bool rcvmsg = false;

        QByteArray arr = receiveData();

        if(!arr.isEmpty())
        {
            str.clear();
            for(int i=0;i<arr.size();i++)
            {
                str.append(arr.at(i));

                if(str=="echo")
                {
                    deltaTime1 = QTime::currentTime();
                    qDebug() << "Recievetime: " + deltaTime1.toString();
                    sendData("\r\necho\r\n");
                    str.clear();
                }
                // Start of message
                else if(str == "!")
                {
                    rcv.clear();
                    str.clear();
                    rcvmsg = true;
                    //rcv.append("\r\n");
                }
                // End of message
                else if(str.contains("\r\n",Qt::CaseSensitive) && rcvmsg)
                {
                    str.clear();
                    rcvmsg = false;

                    // Begin unpack
                    unsigned char* start_memptr;
                    start_memptr = (unsigned char*)malloc(rcv.size()*sizeof(unsigned char));

                    for(int i=0;i<rcv.size();i++)
                    {
                        start_memptr[i]=rcv.at(i).toLatin1();
                    }
                    if(unpackage_BT_message((start_memptr), rcv.size()-1))
                    {
                        switch(buffer[4])
                        {
                        case PANORAMA_PROCESS_DATA:
                            pano_process_data.setData(buffer);
                            break;

                        case CAMERA_DEVICE_DATA:
                            pano_process_data.setCameraDeviceData(buffer);
                            break;

                        case CAMERA_STATUS_DATA:
                            pano_process_data.setCameraStatusData(buffer);
                            break;

                        case PANORAMA_WORK_DATA:
                            pano_process_data.setPanoramaWorkData(buffer);
                            break;

                        default:
                            qDebug() << "Unable to parse Data";
                            break;
                        }
                    }
                }
                else if(str == "\r\n")
                {
                    str.clear();
                }
                // Append characters of received message
                if(rcvmsg)
                {
                    rcv.append(arr.at(i));
                }

            }

        }
    }
}

void Communication::packinbuffer(unsigned char inval)
{
    switch(inval)
    {
    case 0: //'\0'
        buffer[bufferlength++] = '-';
        buffer[bufferlength++] = '0';
        break;
    case 10: //'\n'
        buffer[bufferlength++] = '-';
        buffer[bufferlength++] = 'n';
        break;
    case 13: //'\r'
        buffer[bufferlength++] = '-';
        buffer[bufferlength++] = 'r';
        break;
    case 45: //'\-'
        buffer[bufferlength++] = '-';
        buffer[bufferlength++] = 'e';
        break;
    default:
        buffer[bufferlength++] = inval;
    }
}

unsigned char *Communication::package_BT_message(const unsigned char *inptr, unsigned char inlength, unsigned char channel)
{
    if (inlength > 120) inlength = 120;
    //ermittle pr�fsumme
    unsigned short pruefsumme = 0;
    pruefsumme += channel;
    pruefsumme += inlength;
    for (unsigned char i=0; i<inlength; i++)
    {
        pruefsumme += *(inptr+i);
    }
    unsigned char pwert0 = pruefsumme/256;
    qDebug() << "pwert0: " << pwert0;

    unsigned char pwert1 = pruefsumme%256;
    qDebug() << "pwert1: " << pwert1;
    bufferlength = 0;

    //erstelle buffer
    packinbuffer('!');
    packinbuffer(pruefsumme/256);
    packinbuffer(pruefsumme%256);
    packinbuffer(channel);
    packinbuffer(inlength);
    for (unsigned char i=0;i<inlength;i++)
    {
        packinbuffer(*(inptr+i));
    }
    buffer[bufferlength++] = '\0';

    return buffer;
}

bool Communication::unpackage_BT_message(unsigned char *inptr, unsigned char inlength)
{
    //entpacke in buffer
    if (*inptr != '!') return false;
    bufferlength = 0;
    for (unsigned char i = 1; i<inlength; i++)
    {
        if (*(inptr + i) == '-')
        {
            i++;
            if (*(inptr + i) == '0') buffer[bufferlength++] = '\0';
            if (*(inptr + i) == 'e') buffer[bufferlength++] = '-';
            if (*(inptr + i) == 'r') buffer[bufferlength++] = '\r';
            if (*(inptr + i) == 'n') buffer[bufferlength++] = '\n';
        }
        else
            buffer[bufferlength++] = *(inptr + i);
    }

    //pruefe auf Gueltigkeit
    if (bufferlength > 124) return false;
    unsigned short pruefsumme = 0;
    unsigned short vglpruefsumme = ((unsigned short)buffer[0] * 256) + ((unsigned short)buffer[1]);
    for (unsigned char i = 2; i<bufferlength; i++)
    {
        pruefsumme +=buffer[i];
    }
    if (pruefsumme != vglpruefsumme) return false;
    if (buffer[3] != bufferlength - 4) return false;
    return true;
}

bool Communication::sendData(QByteArray data)
{
    qDebug() << "Send data: " + QString::fromStdString(data.data());
#ifdef ANDROID
    rfcomm->send(data);
#endif
    // IOS

    // Win

    return true;
}

QByteArray Communication::receiveData()
{
    QByteArray arr;
#ifdef ANDROID
    arr = rfcomm->receive(100,2000);
#endif
    // IOS

    // Win

    qDebug() << "Receive data: " + QString::fromStdString(arr.data());
    return arr;
}

unsigned char *Communication::getbufferptr()
{
    return buffer;
}

unsigned char Communication::getbufferlength()
{
    return bufferlength;
}

bool Communication::getConnection()
{
    QTime deltaTime2 = QTime::currentTime();
    qDebug() << "QTime2: " + deltaTime2.toString();
    TimeDelta delta = deltaTime2 - deltaTime1;
    qDebug() << "delta.FromSecondsF(): " + QString::number(delta.InSecondsF());

    if(delta.InSecondsF()<TIMEOUT)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Communication::isConnectionEstablished()
{
#ifdef ANDROID
    return rfcomm->isConnected();
#endif
}

void Communication::prepareProzessSendCmnd(_command_data program, _command_data startCmnd)
{
    QByteArray arr_message;

    // Panoramabewegung
    arr_message.clear();
    arr_message.append("\r");
    arr_message.append("\n");

    unsigned char* packedMessage = 0;
    unsigned char* memptr;
    memptr = (unsigned char*)malloc(program.length*sizeof(unsigned char));

    for(int i=0;i<=program.length;i++)
    {
        memptr[i]=program.getVal(i);
    }

    packedMessage = package_BT_message(&(memptr[1]),program.length-1,3);

    for(int i = 0; i < getbufferlength()-1; i++)
    {
        qDebug() << *packedMessage;
        arr_message.append(*packedMessage);
        packedMessage++;
    }


    arr_message.append("\r");
    arr_message.append("\n");
    sendData(arr_message);


    // Startbefehl
    arr_message.clear();
    arr_message.append("\r");
    arr_message.append("\n");

    unsigned char* start_packedMessage = 0;
    unsigned char* start_memptr;
    start_memptr = (unsigned char*)malloc(startCmnd.length*sizeof(unsigned char));

    for(int i=0;i<=startCmnd.length;i++)
    {
        start_memptr[i]=startCmnd.getVal(i);
    }

    start_packedMessage = package_BT_message(&(start_memptr[1]), startCmnd.length-1,4);

    for(int i = 0; i < getbufferlength()-1; i++)
    {
        qDebug() << *start_packedMessage;
        arr_message.append(*start_packedMessage);
        start_packedMessage++;
    }

    arr_message.append("\r");
    arr_message.append("\n");
    sendData(arr_message);


}

