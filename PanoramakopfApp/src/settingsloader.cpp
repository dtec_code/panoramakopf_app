#include "settingsloader.h"

SettingsLoader::SettingsLoader(QQmlApplicationEngine *engine)
{
    this->engine = engine;
    rootObject = engine->rootObjects().first();
    loadSettings();
    initGui();
}

void SettingsLoader::initGui()
{
    int cameraSize = cameraData.size();
    for(int i=0; i<cameraSize;i++){
        QVariant ele = (QVariant)cameraData.back();
        cameraData.pop_back();
        QString cameraName = ele.toString();
        cameraName = cameraName.split(";").at(0);
        putElementInUi("cameraComboBox", QVariant(cameraName));
    }

    int sensorSize = sensorData.size();
    for(int i=0; i<sensorSize; i++){
        QVariant ele = (QVariant)sensorData.back();
        QString sensorName = ele.toString();
        sensorName = sensorName.split(";").at(0);
        sensorData.pop_back();
        putElementInUi("sensorComboBox",QVariant(sensorName));
    }

}

void  SettingsLoader::loadSettings()
{
#ifdef ANDROID
    QSettings settings("/sdcard/Panoramakopf/settings.ini", QSettings::IniFormat);
#endif

#ifdef IOS
    QSettings settings("/sdcard/Panoramakopf/settings.ini", QSettings::IniFormat);
#endif

    QVariant cameraNum = settings.value("Cameranumber");
    for(int i=0; i<cameraNum.toInt(); i++)
    {
        QString camKey = "Camera";
        camKey.append(QString::number(i));
        cameraData.push_back(settings.value(camKey));

        QStringList strList = settings.value(camKey).toString().split(";");

        cameraDataMap.insert(strList.at(0), strList.at(1));

        // Put full camerainformation into an array in qml-file
        QVariant qVar = QVariant(settings.value(camKey));
        QMetaObject::invokeMethod(rootObject->findChild<QObject*>("cameraComboBox"), "addCameraInformation",
                                  Q_ARG(QVariant, camKey),
                                  Q_ARG(QVariant, qVar.toString()));

    }
    cameraData.push_back(QVariant("Benutzerdefiniert"));

    QVariant sensorNum = settings.value("Sensornumber");
    for(int i=0; i<sensorNum.toInt(); i++)
    {

        QString sensorKey = "Sensor";
        sensorKey.append(QString::number(i));
        sensorData.push_back(settings.value(sensorKey));

        QStringList strList = settings.value(sensorKey).toString().split(";");
        QString val = ((QString)strList.at(1)).append(";");
        val.append(strList.at(2));
        sensorDataMap.insert(strList.at(0), val);

        // Put full sensorinformation into an array in qml-file
        QVariant qVar = QVariant(settings.value(sensorKey));
        QMetaObject::invokeMethod(rootObject->findChild<QObject*>("cameraComboBox"), "addSensorInformation",
                                  Q_ARG(QVariant, sensorKey),
                                  Q_ARG(QVariant, qVar.toString()));

    }

}

void  SettingsLoader::saveSettings()
{    

}
void SettingsLoader::addDataToSettingsFile(QString cameraName, QString sensorName, QString sensorWidth, QString sensorHeight)
{

    // cameraname and sensorname are empty
    if(QString::compare(cameraName, "", Qt::CaseInsensitive) == 0
            && QString::compare(sensorName, "", Qt::CaseInsensitive) == 0)
    {
        return;
    }

#ifdef ANDROID
    QString pathAndFileName = "/sdcard/Panoramakopf/settings.ini";
#endif

#ifdef IOS
    QString pathAndFileName = "/sdcard/Panoramakopf/settings.ini";
#endif

    QSettings settings(pathAndFileName, QSettings::IniFormat);

    QFile file(pathAndFileName);
    file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text);
    QTextStream out(&file);

    QVariant sensorNum;
    QString sensorString;

    // cameraentry with sensor
    if(!(QString::compare(cameraName, "", Qt::CaseInsensitive) == 0
         && QString::compare(sensorName, "", Qt::CaseInsensitive) == 0))
    {
        QVariant cameraNum = settings.value("Cameranumber");
        QString cameraString = "Camera";
        cameraString.append(cameraNum.toString());
        cameraString.append("=");
        cameraString.append("\"");
        cameraString.append(cameraName);
        cameraString.append(";");
        sensorNum = settings.value("Sensornumber");
        sensorString = "Sensor";
        sensorString.append(sensorNum.toString());
        cameraString.append(sensorString);
        cameraString.append("\"");
        out << cameraString;

        sensorNum = settings.value("Sensornumber");
        sensorString.clear();
        sensorString = createSensorString(sensorNum.toString(), sensorName, sensorWidth, sensorHeight);
        // endl is important to get an seperate entry of sensor
        out << endl << sensorString;
        file.close();
        settings.setValue("Cameranumber",cameraNum.toInt()+1);
        settings.setValue("Sensornumber", sensorNum.toInt()+1);
    }
    // if cameraname is not available/empty, then put only the sensor in the settingsfile
    else if((QString::compare(cameraName, "", Qt::CaseInsensitive) == 0)
            && !(QString::compare(sensorName, "", Qt::CaseInsensitive) == 0))
    {
        sensorNum = settings.value("Sensornumber");
        sensorString = createSensorString(sensorNum.toString(), sensorName, sensorWidth, sensorHeight);
        out << sensorString;
        file.close();
        settings.setValue("Sensornumber", sensorNum.toInt()+1);
    }

}

QString SettingsLoader::createSensorString(QString sensorNum, QString sensorName, QString sensorWidth, QString sensorHeight)
{
    QString sensorString = "Sensor";
    sensorString.append(sensorNum);
    sensorString.append("=");
    sensorString.append("\"");
    sensorString.append(sensorName).append(";");
    sensorString.append(sensorWidth).append(";");
    sensorString.append(sensorHeight);
    sensorString.append("\"");
    return sensorString;
}

QString SettingsLoader::pullSensorDataFromSettingsFile(QString cameraName)
{
// lade aus cameraDataMap und sensorDataMap
}

void SettingsLoader::putElementInUi(QString objectElement, QVariant element)
{
    QObject *obj = rootObject->findChild<QObject*>(objectElement);
    QMetaObject::invokeMethod(
                obj,                          // for this object we will call method
                "append",                                         // actually, name of the method to call
                Q_ARG(QVariant, element.toString())  // method parameter
                );

}

QMap<QString, QString> SettingsLoader::getSensorData()
{
    return sensorDataMap;
}
