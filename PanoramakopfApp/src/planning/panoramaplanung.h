#ifndef PANORAMAPLANUNG_H
#define PANORAMAPLANUNG_H

#define MOTOR_ANGLE 0.18

struct _command_data
{
    unsigned char data[256];
    int length = 0;

    int gw_v = 0;
    int gw_h = 0;
    int count_h = 0;
    int count_v = 0;
    int rest_h =  0;
    int rest_v =  0;
    int n_h    =  0;
    int n_v    =  0;
    int steps_hr = 0;       //steps per repetition in horizontal direction
    int steps_h_total = 0;  //steps total in horizontal direction
    int steps_vr = 0;
    int steps_v_total = 0;  //steps total in vertical direction
    float rotation_angle_h = 0;
    float rotation_angle_v = 0;

    unsigned char getVal(int val)
    {
        return data[val];
    }

    void addCommand(unsigned char command)
    {
        data[length] = command;
        length++;
    }

    int getLength()
    {
        return length;
    }

    void setCountH(int val)
    {
        count_h = val;
    }

    void setCountV(int val)
    {
        count_v = val;
    }

    void setRestH(int val)
    {
        rest_h = val;
    }

    void setRestV(int val)
    {
        rest_v = val;
    }

    void setWhgH(int val)
    {
        n_h = val;
    }

    void setWhgV(int val)
    {
        n_v = val;
    }

    void setStepsHoriTotal(int val)
    {
        steps_h_total = val;
    }

    void setStepsVertTotal(int val)
    {
        steps_v_total = val;
    }

    void setStepsH(int val)
    {
        steps_hr = val;
    }

    void setStepsV(int val)
    {
        steps_vr = val;
    }

    void setGwH(int val)
    {
        gw_h = val;
    }
    void setGwV(int val)
    {
        gw_v = val;
    }

    void setRotationAngle_H(float val)
    {
        rotation_angle_h = val;

    }

    void setRotationAngle_V(float val)
    {
        rotation_angle_v = val;

    }

    int getCountH()
    {
        return count_h ;
    }

    int getCountV()
    {
        return count_v;
    }

    int getRestH()
    {
        return rest_h;
    }

    int getRestV()
    {
        return rest_v ;
    }

    int getWhgH()
    {
        return n_h;
    }

    int getWhgV()
    {
        return n_v ;
    }
    int getStepsH()
    {
        return steps_hr ;
    }
    int getStepsV()
    {
        return steps_vr ;
    }
    int getGwH()
    {
        return gw_h;
    }
    int getGwV()
    {
        return gw_v;
    }
    int getStepsHoriTotal()
    {

        return steps_h_total;
    }
    int getStepsVertTotal()
    {
        return steps_v_total;
    }
    float getRotationAngle_H()
    {
        return rotation_angle_h;
    }

    float getRotationAngle_V()
    {
        return rotation_angle_v;
    }
};

class Panoramaplanung
{

public:
    Panoramaplanung();

public:
    _command_data doZylinderpanorama(float hoehe_verti, float hoehe_hori, float brennweite, float ue_gra, float wink_hori, float wink_vert);
    _command_data doKugelpanorama(float hoehe_verti, float hoehe_hori, float brennweite, float ue_gra);
    _command_data startPanoramapgram();
    _command_data stopPanoramaprogram();
    _command_data setCamera();
    _command_data triggerCamera();
    _command_data fahreAufSensorBeginneZaehlen();
    _command_data uebernehmeOffsetStoppZaehlen();
    _command_data fahreOffset();


    _command_data goVerticalLeft(_command_data pano, int steps);
    _command_data goVerticalLeft(float angle);

    _command_data goVerticalRight(_command_data pano , int steps);
    _command_data goVerticalRight(float angle);

    _command_data goHorizontalRight(_command_data pano, int steps);
    _command_data goHorizontalRight(float angle);

    _command_data goHorizontalLeft(_command_data pano, int steps);
    _command_data goHorizontalLeft(float angle);

    _command_data panoFahrt(_command_data pano,int command);
    _command_data startePanorama(_command_data pano);
    _command_data horizontaleFahrt(_command_data pano);
    _command_data vertikaleFahrt(_command_data pano, int command);


};

#endif // PANORAMAPLANUNG_H
