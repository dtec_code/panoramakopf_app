﻿
#include "panoramaplanung.h"
#include <QDebug>
#include <math.h>
#include "utils.cpp"

#define MO 0.18
//test

short memaddr;
Panoramaplanung::Panoramaplanung()
{
}

_command_data Panoramaplanung::doZylinderpanorama(float hoehe_verti, float hoehe_hori, float brennweite, float ue_gra, float wink_hori, float wink_vert)
{
    //float h_h     = 14.9;                             //Hoehe Objektiv wichtig fuer horizontalen Bildwinkel 14.9
    //float h_v     = 22.3;                             //Breite Objektiv fuer vertikalen Bildwinkel
    //int f         = 16;                               //Brennweite
    //float ue_grad = 0.3;
    //float gw_h    = 120;                              //Gesamtwinkel horizontal ist immer 360
    //float gw_v    = 120;                              //Gesamtwinkel vertikal ist immer 180

    float h_h     = hoehe_hori;                         //Hoehe Objektiv wichtig fuer horizontalen Bildwinkel 14.9
    float h_v     = hoehe_verti;                        //Breite Objektiv fuer vertikalen Bildwinkel
    int f         = brennweite;                         //Brennweite
    float ue_grad = ue_gra;
    float gw_h    = wink_hori * 2;                      //Gesamtwinkel horizontal ist immer 360
    float gw_v    = wink_vert * 2;                      //Gesamtwinkel vertikal ist immer 180

    float gw_vh   = gw_v /2;                            //Gesamtwinkel horizontal halbiert fuer Fahrt nachoben und unten
    float bw_h;                                         //Bildwinkel horizontal
    float bw_v;                                         //Bildwinkel vertikal
    float steps_h;                                      //Steps horizontal
    float steps_v;                                      //Steps vertikal

    int steps_vr_n;                                     //Steps gerundet pro Abschnitt vertikal
    int steps_hr_n;

    int n_hr;
    int n_vr;

    int count_h;
    int rest_h;
    int count_v;
    int rest_v;

    _command_data pano;

    bw_h = computeAngle(h_h, f);                        //Bildwinkel horizontal
    qDebug() << "bw horizontal" << bw_h;

    bw_v = computeAngle(h_v, f) ;                       //Bildwinkel vertikal
    qDebug() << "bw vertikal" << bw_v;

    if(gw_v < bw_v )                                    //gw_v mindestens auf Bildwinkel setzen
    {
        gw_v = bw_v;
    }

    if(gw_h < bw_h )                                    //gw horizontal mindestens auf Bildwinkel setzen
    {
       gw_h = bw_h;
    }

    steps_h  = computeSteps(gw_h - (bw_h * (1- ue_grad )));             //Steps in horizontaler richtung Gesamtwinkel horizontal - Bildwinkel
    qDebug() << "steps horizontal insgesamt"<< steps_h;
    pano.setStepsHoriTotal(steps_h);

    steps_v  = computeSteps(gw_vh - ((bw_v * (1- ue_grad)) / 2));        //Steps in vertikaler Richtung allerdings nur in eine Richtung von Startposition aus
    pano.setStepsVertTotal(steps_v);

    qDebug() << "steps vertikal insgesamt"<< steps_v;


    n_hr = computeRepetition(gw_h, ue_grad, bw_h);                          //Anzahl an Bilder horizontal
    pano.setWhgH(n_hr);
    qDebug() << "n horizontal" << n_hr ;

    n_vr  =  computeRepetition((gw_vh + bw_v/2), ue_grad, bw_v);            //Anzahl der Stufen vertikal
    pano.setWhgV(n_vr);
    qDebug() << "n vertikal" << n_vr ;

    steps_hr_n = computeStepsPerRep(steps_h, n_hr);             //schritte vor jedem Ausloeser horizontal
    qDebug() << "steps_hr horizontal " << steps_hr_n;
    pano.setStepsH(steps_hr_n);
    float rot_angle_h = float(steps_hr_n * MOTOR_ANGLE);
    pano.setRotationAngle_H(rot_angle_h);

    steps_vr_n = computeStepsPerRep(steps_v, n_vr);             //Schritte vor jedem Asuloeser vertikal
    qDebug() << "steps_vh vertikal " << steps_vr_n;
    pano.setStepsV(steps_vr_n);
    float rot_angel_v = float(steps_vr_n * MOTOR_ANGLE);
    pano.setRotationAngle_V(rot_angel_v);

    count_h = computeCounter(steps_hr_n);                       //Anzahl von wievielen Durchlaufen mit pro Step Durchlauf hori
    pano.setCountH(count_h);
    qDebug() << "counter_h hori " << count_h;

    rest_h    = computeRest(steps_hr_n);
    pano.setRestH(rest_h);
    qDebug() << "rest_h hori " << rest_h;

    count_v = computeCounter(steps_vr_n);                    //Anzahl von wievielen Durchlaufen mit pro Step Durchlauf verti
    qDebug() << "counter_v verti " << count_v;
    pano.setCountV(count_v);

    rest_v    = computeRest(steps_vr_n);
    qDebug() << "rest_h hori " << rest_v;
    pano.setRestV(rest_v);


    pano.addCommand(3);

    if(n_vr < 1 && n_hr <= 1)
    {
        pano = startePanorama(pano);

    }
    else if(n_vr < 1)
    {
        pano = startePanorama(pano);
        pano = horizontaleFahrt(pano);
    }

    else if(n_hr <= 1)
    {
        qDebug() << "vert. Panorama";
        pano = startePanorama(pano);
        //int steps = pano.getWhgV() * pano.getStepsV();
        qDebug() << "Steps wieder vertikal zurück" << pano.getStepsVertTotal();
        pano = vertikaleFahrt(pano, 238);
        pano = goVerticalLeft(pano, pano.getStepsVertTotal());
        pano = vertikaleFahrt(pano, 237);
        pano = goVerticalRight(pano, pano.getStepsVertTotal());
    }
    else
    {
        qDebug() << "norm. Zy Panorama";
        pano = startePanorama(pano);
        pano = horizontaleFahrt(pano);
        pano = goHorizontalLeft(pano, pano.getStepsHoriTotal());
        pano = panoFahrt(pano, 238);
        pano = goVerticalLeft(pano, pano.getStepsVertTotal());
        pano = panoFahrt(pano, 237);
    }

    pano.addCommand(241);
    pano.addCommand(255);

    int l1 = pano.getLength();
    qDebug() << "Länge" << l1;

    return pano;

}

_command_data Panoramaplanung::doKugelpanorama(float hoehe_verti, float hoehe_hori, float brennweite, float ue_gra)
{
    int i;
    //float h_h     = 14.9;                               //Hoehe Objektiv wichtig fuer horizontalen Bildwinkel 14.9
    //float h_v     = 22.3;                               //Breite Objektiv fuer vertikalen Bildwinkel
    //int f         = 16;                                 //Brennweite
    //float ue_grad = 0.3;
    float gw_h    = 360;                                //Gesamtwinkel horizontal ist immer 360
    float gw_v    = 180;                                //Gesamtwinkel vertikal ist immer 180

    float h_h     = hoehe_hori;                         //Hoehe Objektiv wichtig fuer horizontalen Bildwinkel 14.9
    float h_v     = hoehe_verti;                        //Breite Objektiv fuer vertikalen Bildwinkel
    int f         = brennweite;                         //Brennweite
    float ue_grad = ue_gra;

    float gw_vh   = gw_v /2;                            //Gesamtwinkel horizontal halbiert fuer Fahrt nachoben und unten
    float bw_h;                                         //Bildwinkel horizontal
    float bw_v;                                         //Bildwinkel vertikal
    float steps_h;                                      //Steps horizontal
    float steps_v;                                      //Steps vertikal

    int steps_vr_n;                                     //Steps gerundet pro Abschnitt vertikal
    int steps_hr_n;

    int n_hr;
    int n_vr;

    int count_h;
    int rest_h;
    int count_v;
    int rest_v;

    _command_data pano;

    pano.setGwH(gw_h);
    pano.setGwV(gw_v);

    bw_h = computeAngle(h_h, f);                        //Bildwinkel horizontal
    qDebug() << "bw horizontal" << bw_h;

    bw_v = computeAngle(h_v, f) ;                       //Bildwinkel vertikal
    qDebug() << "bw vertikal" << bw_v;

    steps_h  = computeSteps(gw_h);                             //Steps in horizontaler richtung
    qDebug() << "steps horizontal insgesamt"<< steps_h;
    pano.setStepsHoriTotal(steps_h);

    steps_v  = computeSteps(gw_vh);                            //Steps in vertikaler Richtung nur in 1 Richtung haelfte des Gesamtwinkels
    qDebug() << "steps vertikal insgesamt"<< steps_v;
    pano.setStepsVertTotal(steps_v);

    n_hr = computeRepetition((gw_h + bw_h) , ue_grad, bw_h);              //Anzahl an Bilder horizontal
    pano.setWhgH(n_hr);
    qDebug() << "n horizontal" << n_hr ;

    n_vr  =  computeRepetition((gw_vh + bw_v/2), ue_grad, bw_v);            //Anzahl der Bilder vertikal
    pano.setWhgV(n_vr - 1);
    qDebug() << "n vertikal" << (n_vr-1) ;

    steps_hr_n = computeStepsPerRep(steps_h, n_hr);             //schritte vor jedem Ausloeser horizontal
    qDebug() << "steps_hr horizontal " << steps_hr_n;
    pano.setStepsH(steps_hr_n);
    float rot_h = steps_hr_n * MOTOR_ANGLE;
    pano.setRotationAngle_H(rot_h);
     qDebug() << "Rotation angle " << rot_h;

    steps_vr_n = computeStepsPerRep(steps_v, n_vr);             //Schritte vor jedem Asuloeser vertikal
    qDebug() << "steps_vh vertikal " << steps_vr_n;

    pano.setStepsV(steps_vr_n);
    float rot_v  = (float)steps_vr_n * MOTOR_ANGLE;
    pano.setRotationAngle_V(rot_v);
    qDebug() << "Rotation angle " << float(rot_v);
    qDebug() << "motor Angle " << pano.getRotationAngle_V();

    count_h = computeCounter(steps_hr_n);                             //Anzahl von wievielen Durchlaufen mit pro Step Durchlauf hori
    pano.setCountH(count_h);
    qDebug() << "counter_h hori " << count_h;

    rest_h    = computeRest(steps_hr_n);
    pano.setRestH(rest_h);
    qDebug() << "rest_h hori " << rest_h;

    count_v = computeCounter(steps_vr_n);                    //Anzahl von wievielen Durchlaufen mit pro Step Durchlauf verti
    qDebug() << "counter_v verti " << count_v;
    pano.setCountV(count_v);

    rest_v    = computeRest(steps_vr_n);
    qDebug() << "rest_h hori " << rest_v;
    pano.setRestV(rest_v);

    pano.addCommand(3);
    pano = startePanorama(pano);
    pano = horizontaleFahrt(pano);
    pano = panoFahrt(pano, 238);
    pano = goVerticalLeft(pano, (n_vr-1) * steps_vr_n);
    pano = panoFahrt(pano, 237);
    pano = goVerticalLeft(pano, steps_vr_n );

    pano.addCommand(241);
    pano.addCommand(255);

    int l1 = pano.getLength();
    qDebug() << "Länge" << l1;

    return pano;
}

_command_data Panoramaplanung::startePanorama(_command_data pano)
{
    //Fahrt zur Null Position
    pano.addCommand(236);
    pano.addCommand(244);
    pano.addCommand(237);
    pano.addCommand(250);
    pano.addCommand(1);
    pano.addCommand(237);
    pano.addCommand(220);
    pano.addCommand(1);
    return pano;
}

_command_data Panoramaplanung::panoFahrt(_command_data pano,int command)
{
    pano.addCommand(229);
    int length_1 = pano.getLength();
    pano.addCommand(pano.getWhgV());


    if(pano.getCountV() <= 2)
    {
        for(int i = 0; i< pano.getCountV(); i++)
        {
            pano.addCommand(command);
            pano.addCommand(254);
            pano.addCommand(1);
        }
    }

   if(pano.getCountV() > 2)
    {
        pano.addCommand(230);
        int length_3 = pano.getLength();
        pano.addCommand(pano.getCountV());
        pano.addCommand(command);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(234);
        pano.addCommand(128 + length_3);

    }

    pano.addCommand(command);
    pano.addCommand(pano.getRestV());
    pano.addCommand(1);


    //horizontal
    pano.addCommand(228);
    int length_2 = pano.getLength();
    pano.addCommand(pano.getWhgH());
    pano.addCommand(241);


    if(pano.getCountH()<= 2)
    {
        for(int i = 0; i< pano.getCountH(); i++)
        {
            pano.addCommand(239);
            pano.addCommand(254);
            pano.addCommand(1);
        }
    }

    else
    {
         pano.addCommand(230);
         int length_3 = pano.getLength();
         pano.addCommand(pano.getCountH());
         pano.addCommand(239);
         pano.addCommand(254);
         pano.addCommand(1);
         pano.addCommand(234);
         pano.addCommand(128 + length_3);

     }

    pano.addCommand(239);
    pano.addCommand(pano.getRestH());
    pano.addCommand(1);



    pano.addCommand(232);
    pano.addCommand(128 + length_2); //???

    pano.addCommand(241);

    int steps = pano.getWhgH() * pano.getStepsH();
    qDebug() << "steps in pano fahrt" << steps;

    if(pano.getStepsHoriTotal() != 2000)
    {
        pano = goHorizontalLeft(pano, steps);
    }


    //Ende horizontal

    pano.addCommand(233);
    pano.addCommand(128 + length_1);
    return pano;
}

_command_data Panoramaplanung::horizontaleFahrt(_command_data pano)
{

    //horizontal
    pano.addCommand(228);
    int length_2 = pano.getLength();
    pano.addCommand(pano.getWhgH());

    pano.addCommand(241);


    if(pano.getCountH()<= 2)
    {
        for(int i = 0; i< pano.getCountH(); i++)
        {
            pano.addCommand(239);
            pano.addCommand(254);
            pano.addCommand(1);
        }
    }

    if(pano.getCountH() > 2)
     {
         pano.addCommand(230);
         int length_3 = pano.getLength();
         pano.addCommand(pano.getCountH());
         pano.addCommand(239);
         pano.addCommand(254);
         pano.addCommand(1);
         pano.addCommand(234);
         pano.addCommand(128 + length_3);

     }

    pano.addCommand(239);
    pano.addCommand(pano.getRestH());
    pano.addCommand(1);

    pano.addCommand(232);
    pano.addCommand(128 + length_2); //???

    pano.addCommand(241);



    return pano;
}

_command_data Panoramaplanung::vertikaleFahrt(_command_data pano, int command)
{

    qDebug() <<"vertikale Fahrt";
    //vertikal fahren
    pano.addCommand(228);
    int length_2 = pano.getLength();
    pano.addCommand(pano.getWhgV());
    pano.addCommand(241);


    if(pano.getCountV()<= 2)
    {
        for(int i = 0; i< pano.getCountV(); i++)
        {
            pano.addCommand(command);
            pano.addCommand(254);
            pano.addCommand(1);
        }
    }
    if(pano.getCountV() > 0)
     {
         pano.addCommand(230);
         int length_3 = pano.getLength();
         pano.addCommand(pano.getCountV());
         pano.addCommand(command);
         pano.addCommand(254);
         pano.addCommand(1);
         pano.addCommand(234);
         pano.addCommand(128 + length_3);

     }

    pano.addCommand(command);

    if(pano.getRestV()== 0)
    {
        pano.addCommand(1);
    }
    else
    {
    pano.addCommand(pano.getRestV());
    }
    pano.addCommand(1);

    pano.addCommand(232);
    pano.addCommand(128 + length_2); //???


    pano.addCommand(241);

    return pano;
}

_command_data Panoramaplanung::startPanoramapgram()
{
    _command_data pano;

    pano.addCommand(4);
    pano.addCommand(1);

    return pano;
}

_command_data Panoramaplanung::stopPanoramaprogram()
{
    _command_data pano;

    pano.addCommand(4);
    pano.addCommand(0);

    return pano;
}

_command_data Panoramaplanung::setCamera()
{
    _command_data pano;

    pano.addCommand(3);
    pano.addCommand(236);
    pano.addCommand(244);
    pano.addCommand(237);
    pano.addCommand(250);
    pano.addCommand(1);
    pano.addCommand(237);
    pano.addCommand(250);
    pano.addCommand(1);

    pano.addCommand(255);

    return pano;
}

_command_data Panoramaplanung::triggerCamera()
{
        _command_data pano;

        pano.addCommand(3);
        pano.addCommand(241);
        pano.addCommand(255);

        return pano;
}

_command_data Panoramaplanung::fahreAufSensorBeginneZaehlen(){
    _command_data pano;

    pano.addCommand(3);
    pano.addCommand(236);
    pano.addCommand(249);
    pano.addCommand(239);
    pano.addCommand(10);
    pano.addCommand(1);
    pano.addCommand(240);
    pano.addCommand(10);
    pano.addCommand(1);
    pano.addCommand(255);

    return pano;
}

_command_data Panoramaplanung::uebernehmeOffsetStoppZaehlen(){

    _command_data pano;

    pano.addCommand(3);
    pano.addCommand(243);
    pano.addCommand(237);
    pano.addCommand(55);
    pano.addCommand(1);
    pano.addCommand(255);

    return pano;
}

_command_data Panoramaplanung::fahreOffset(){

    _command_data pano;

    pano.addCommand(3);
    pano.addCommand(244);
    pano.addCommand(255);

    return pano;
}

_command_data Panoramaplanung::goVerticalLeft(_command_data pano, int steps)
{
    int i;
    float n = steps / 254;
    int n_r = n;
    int rest = steps % 254;

    if(n > 0)
    {
        pano.addCommand(228);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(237);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(232);
        pano.addCommand(128 + length);
    }


    pano.addCommand(237);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    return pano;
}

_command_data Panoramaplanung::goVerticalRight(_command_data pano, int steps)
{
    int i;
    float n = steps / 254;
    int n_r = n;
    int rest = steps % 254;

    if(n > 0)
    {
        pano.addCommand(228);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(238);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(232);
        pano.addCommand(128 + length);
    }


    pano.addCommand(238);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    return pano;
}

_command_data Panoramaplanung::goVerticalLeft(float angle)
{
    _command_data pano;
    int i;
    float steps = angle / 0.18;
    float n = steps / 254;
    int n_r = n;
    //int steps = angle;
    //int n_r = steps/254;
    qDebug() << "n" << n_r;
    int rest = (int)steps % 254;

    pano.addCommand(3);
    if(rest == 0)
    {
        rest = 1;
    }

    qDebug() << "rest" << rest;

    if(n_r > 0)
    {
        pano.addCommand(228);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(237);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(232);
        pano.addCommand(128 + length);
    }


    pano.addCommand(237);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    pano.addCommand(255);
    return pano;

}

_command_data Panoramaplanung::goVerticalRight(float angle)
{
    _command_data pano;
    int i;
    float steps = angle / 0.18;
    float n = steps / 254;
    int n_r = n;
    //int steps = angle;
    //int n_r = steps/254;
    qDebug() << "n" << n_r;
    int rest = (int) steps % 254;

    pano.addCommand(3);
    if(rest == 0)
    {
        rest = 1;
    }

    qDebug() << "rest" << rest;

    if(n_r > 0)
    {
        pano.addCommand(228);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(238);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(232);
        pano.addCommand(128 + length);
    }


    pano.addCommand(238);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    pano.addCommand(255);
    return pano;

}

_command_data Panoramaplanung::goHorizontalLeft(_command_data pano, int steps)
{
    int i;
    float n = steps / 254;
    int n_r = n;
    int rest = steps % 254;

    qDebug() << "Steps hori zurueck" << steps;

//hier muss man noch etwas tun :-)

    if(n > 0)
    {
        pano.addCommand(230);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(240);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(234);
        pano.addCommand(128 + length);
    }



    pano.addCommand(240);
    pano.addCommand(rest-1);
    pano.addCommand(1);


    return pano;
}

_command_data Panoramaplanung::goHorizontalRight(float angle)
{
    _command_data pano;
    int i;
    float steps = angle / 0.18;
    float n = steps / 254;
    int n_r = n;
    //int steps = angle;
    //int n_r = steps/254;
    qDebug() << "n" << n_r;
    int rest = (int)steps % 254;

    pano.addCommand(3);
    if(rest == 0)
    {
        rest = 1;
    }

    qDebug() << "rest" << rest;

    if(n_r > 0)
    {
        pano.addCommand(228);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(239);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(232);
        pano.addCommand(128 + length);
    }


    pano.addCommand(239);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    pano.addCommand(255);
    return pano;

}

_command_data Panoramaplanung::goHorizontalRight(_command_data pano, int steps)
{
    int i;
    float n = steps / 254;
    int n_r = n;
    int rest = steps % 254;

    qDebug() << "Steps hori zurueck" << steps;

    if(n > 0)
    {
        pano.addCommand(230);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(239);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(234);
        pano.addCommand(128 + length);
    }


    pano.addCommand(239);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    return pano;
}

_command_data Panoramaplanung::goHorizontalLeft(float angle)
{
    _command_data pano;
    int i;
    float steps = angle / 0.18;
    float n = steps / 254;
    int n_r = n;
    //int steps = angle;
    //int n_r = steps/254;
    qDebug() << "n" << n_r;
    int rest = (int)steps % 254;

    pano.addCommand(3);
    if(rest == 0)
    {
        rest = 1;
    }

    qDebug() << "rest" << rest;

    if(n_r > 0)
    {
        pano.addCommand(228);
        int length = pano.getLength();
        pano.addCommand(n_r);
        pano.addCommand(240);
        pano.addCommand(254);
        pano.addCommand(1);
        pano.addCommand(232);
        pano.addCommand(128 + length);
    }


    pano.addCommand(240);
    pano.addCommand(rest-1);
    pano.addCommand(1);

    pano.addCommand(255);
    return pano;

}

