#include <QDebug>
#include <math.h>
#include "panoramaplanung.h"

float computeAngle(float h, float f)
{
    float bw;
    bw = (2 * atan(h/(2*f))) * 180 /M_PI ;          //Bildwinkel berechne
    return bw;
}

float computeSteps(float angle)
{
    float steps = 0;
    steps = angle/MOTOR_ANGLE;
    return steps;

}

int computeRepetition(float gw, float ue_grad, float bw)
{
    float rep;
    int rep_r;
    rep = (gw - (ue_grad* bw)) / (bw- (ue_grad * bw));
    rep_r = rep + 0.5;
    return rep_r;
}

int computeStepsPerRep(int steps, int rep)
{
    int stepsPR;
    float stepsPR_r;

    if(rep == 0)
    {
        return 0;
    }

    stepsPR= steps / rep;                             //schritte vor jedem Ausloeser horizontal
    stepsPR_r = stepsPR + 0.5;

    return stepsPR_r;

}

int computeCounter(int steps)
{
    int count;
    count = steps / 254;
    return count;
}

int computeRest(int steps)
{
    int rest;
    rest = steps % 254;
    return rest;
}
