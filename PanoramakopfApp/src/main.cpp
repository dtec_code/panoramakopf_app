/**
* @file
* @author Kirill Wetoschkin
* @version 1.0
*/

#include "mainwidget.h"
#include "settingsloader.h"
#include "qtquickcontrolsapplication.h"
#include <QtQml/QQmlApplicationEngine>


int main(int argc, char *argv[])
{

    QtQuickControlsApplication app(argc, argv);
    QQmlApplicationEngine engine(QUrl("qrc:///qml///Homescreen.qml"));
    SettingsLoader *settingsLoader = new SettingsLoader(&engine);
    MainWidget *widg = new MainWidget(&engine, settingsLoader);
    return app.exec();

}
