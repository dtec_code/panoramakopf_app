#ifndef SETTINGSLOADER_H
#define SETTINGSLOADER_H

#include "osdefine.h"
#include <QSettings>
#include <QtQml/QQmlApplicationEngine>
#include <QObject>
#include <vector>
#include <QMetaObject>
#include <QStringList>
#include <QMap>
#include <QFile>
#include <QTextStream>

class SettingsLoader
{
public:
    SettingsLoader(QQmlApplicationEngine *engine);

private:
    QQmlApplicationEngine *engine;
    QObject *rootObject;
    QString *cameraList;
    std::vector<QVariant> cameraData;
    QMap<QString, QString> cameraDataMap; // structure: key: Cameraname, value: Sensorkey
    QMap<QString, QString> sensorDataMap; // structure: key: Sensorname, value: Sensorhight;Sensorwidth
    std::vector<QVariant> sensorData; // structure: Sensorname, Sensorhight, Sensorwidth

    void initGui();
    void loadSettings();
    void saveSettings();
    void putElementInUi(QString objectElement, QVariant element);
    QString pullSensorDataFromSettingsFile(QString cameraName);
    QString createSensorString(QString sensorNum, QString sensorName, QString sensorWidth, QString sensorHeight);

public:
    QMap<QString, QString> getSensorData();
    void addDataToSettingsFile(QString cameraName, QString sensorName, QString sensorWidth, QString sensorHeight);

};



#endif // SETTINGSLOADER_H
