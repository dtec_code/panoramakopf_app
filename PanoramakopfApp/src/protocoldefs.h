//------------------------------------------------------------
// Description:
//  Die Definition der Protocollnummern für die Statusinformationen
//
// Author:
//  Daniel Bauer
// Editiert:
//
//
// Date:
//  08.10.2014
//------------------------------------------------------------

#ifndef _protocoldefs_h
#define _protocoldefs_h

// Panoramaprozess
#define PANORAMA_PROCESS_DATA 40
// Aufbau: x-x-3-x-40-command_nowindex-commandlength-nowcommand

// command_nowindex -> Aktueller Index
// commandlength    -> Maximaler Index
// nowcommand       -> Aktueller Befehl

// Panoramastatus
#define PANORAMA_WORK_DATA 43
// Aufbau: x-x-3-x-43-worktodo

// worktodo = 101 -> Es sind noch Aufgaben abzuarbeiten
//          = 102 -> Keine Aufgaben abzuarbeiten

// Kamera Device Informationen
#define CAMERA_DEVICE_DATA 41
// Aufbau: x-x-3-x-41-stateConnected-DeviceData.....

// stateConnected   = 0 -> Kamera nicht über USB verbunden
//                  = 1 -> Kamera über USB verbunden

// DeviceData       = Getrennt mit ';'
//                      - Manufacturer
//                      - Model
//                      - Device Ver.
//                      - Serial Num.

// Kamera Status Informationen
#define CAMERA_STATUS_DATA 42
// Aufbau: x-x-3-x-42-stateConnected-Capture

// stateConnected   = 0 -> Kamera nicht über USB verbunden
//                  = 1 -> Kamera über USB verbunden

// Capture  = 0 -> Alles IO
//          = 1 -> Bild aufgenommen
//          = 2 -> Fehler, Bild nicht aufgenommen

#endif
