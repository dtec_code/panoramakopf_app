#include "mainwidget.h"


MainWidget::MainWidget(QQmlApplicationEngine *engine, SettingsLoader *settingsLoader, QWidget *parent) :  QWidget(parent)
{

    rootObject = engine->rootObjects().first();
    this->settingsLoader = settingsLoader;

    connectionButton = rootObject->findChild<QObject*>("buttonLeft");
    QObject::connect(connectionButton, SIGNAL(clicked()), this, SLOT(clickLeftButton()));

    QObject *recordButton = rootObject->findChild<QObject*>("startCommand");
    QObject::connect(recordButton, SIGNAL(clicked()), this, SLOT(beginRecording()));

    QObject *startSingleCmnd = rootObject->findChild<QObject*>("startSingleCommand");
    QObject::connect(startSingleCmnd, SIGNAL(clicked()), this, SLOT(beginSingleCommand()));

    addCamera_SensorButton = rootObject->findChild<QObject*>("buttonUebernehmen");
    QObject::connect(addCamera_SensorButton, SIGNAL(clicked()), this, SLOT(addCameraData_SensorData()));

    panoramaplanung = new Panoramaplanung();

}


void MainWidget::clickLeftButton()
{

    QVariant connectonButtonText = connectionButton->property("text");
    QString text = connectonButtonText.toString();
    if((QString::compare(text, "Verbinden", Qt::CaseInsensitive) == 0) )
    {
        timerConnectToPanoramaHead = new QTimer(this);
        checkProgress = new QTimer(this);
        communication = new Communication();
        workerThread = new QThread();
        communication->moveToThread(workerThread);
        workerThread->start();
        timerConnectToPanoramaHead->start();
        timerConnectToPanoramaHead->setInterval(750);
        connect(timerConnectToPanoramaHead, SIGNAL(timeout()), this, SLOT(connectionStatusBlink()));
        checkProgress->start();
        checkProgress->setInterval(1000);
        connect(checkProgress, SIGNAL(timeout()), this, SLOT(checkProgressStatus()));


        QMetaObject::invokeMethod(communication, "connectToDevice", Qt::QueuedConnection);

        connectionButton->setProperty("text","Trennen");

    }
    if((QString::compare(text, "Trennen", Qt::CaseInsensitive) == 0))
    {
        disconnectingProcedure();
    }

}

void MainWidget::addCameraData_SensorData()
{

    QString cameraName = rootObject->findChild<QObject*>("kameraNameTextField")->property("text").toString();
    QString sensorName = rootObject->findChild<QObject*>("sensorNameTextField")->property("text").toString();
    QString sensorWidth = rootObject->findChild<QObject*>("sensorBreiteTextField")->property("text").toString();
    QString sensorHight = rootObject->findChild<QObject*>("sensorHoeheTextField")->property("text").toString();

    settingsLoader->addDataToSettingsFile(cameraName, sensorName, sensorWidth, sensorHight);
    //settingsLoader->addDataToSettingsFile("abc", "xyz", "16", "9");


}


void MainWidget::beginRecording()
{

    if(communication != NULL)
    {

        // is connection existing
        if(communication->getConnection() && communication->isConnectionEstablished())
        {
            _command_data program;
            _command_data command;

            float sensorHeight;
            float sensorWidth;
            float focalLength;

            float rotationAngleHorizontal;
            float rotationAngleVertical;

            QMap<QString, QString> sensorDataMap = settingsLoader->getSensorData();

            // read necessary values from gui
            QVariant panoramaKindComboBox = rootObject->findChild<QObject*>("panoramaKindComboBox")->property("currentText");
            QString selectedPanoramakind = panoramaKindComboBox.toString();

            QVariant overlayDegreeText = rootObject->findChild<QObject*>("overlayDegreeComboBox")->property("currentText");
            QString overlayDegree = overlayDegreeText.toString();
            overlayDegree = overlayDegree.left(2); //remove degree-symbol

            QString focalLengthText = rootObject->findChild<QObject*>("focalLength")->property("text").toString();
            // show messagedialog if focalLength is empty
            if(QString::compare(focalLengthText, "", Qt::CaseInsensitive) == 0){
                showMessageDialog(1);
                return;
            }
            focalLength = focalLengthText.toFloat();

            // read sensordata
            QVariant sensorComboBox = rootObject->findChild<QObject*>("sensorComboBox")->property("currentText");
            QString sensor = sensorComboBox.toString();
            QString sensorGroesse = sensorDataMap.value(sensor);
            sensorWidth = sensorGroesse.split(";").at(0).toFloat();
            sensorHeight = sensorGroesse.split(";").at(1).toFloat();


            // Kugelpanorama is selected
            if(QString::compare(selectedPanoramakind, "Kugel", Qt::CaseInsensitive) == 0)
            {
                program = panoramaplanung->doKugelpanorama(sensorWidth, sensorHeight, focalLength, overlayDegree.toFloat()/100);
                rotationAngleHorizontal = program.getRotationAngle_H();
                rotationAngleVertical = program.getRotationAngle_V();
            }
            else //Zylinderpanorama
            {
                // read necessary values from gui
                QVariant horizontalVal = rootObject->findChild<QObject*>("horizSpinBox")->property("value");
                QVariant vertikalVal = rootObject->findChild<QObject*>("vertSpinBox")->property("value");

                program = panoramaplanung->doZylinderpanorama(sensorWidth, sensorHeight,focalLength, overlayDegree.toFloat()/100, horizontalVal.toFloat(), vertikalVal.toFloat());
                rotationAngleHorizontal = program.getRotationAngle_H();
                rotationAngleVertical = program.getRotationAngle_V();

            }
            command = panoramaplanung->startPanoramapgram();
            // final steps will do before it starts
            communication->prepareProzessSendCmnd(program, command);

            // set rotationAngle of vertical and horizontal in gui
            rootObject->findChild<QObject*>("horizontalRotationAngle")->setProperty("text", QString::number(rotationAngleHorizontal,'f', 2));
            rootObject->findChild<QObject*>("verticalRotationAngle")->setProperty("text", QString::number(rotationAngleVertical,'f', 2));

        }else{
            showMessageDialog(0);
        }
    }else{
        showMessageDialog(0);
    }

    //Horizontaler Drehwinkel
    //command.getRotationAngle_H()
    //Vertikaler Drehwinkel
    //command.getRotationAngle_V()

}

void MainWidget::beginSingleCommand()
{
    if(communication != NULL)
    {
        // is connection existing
        if(communication->getConnection())
        {

            // 0: rotateX1 = rotate X axis in left direction
            // 1: rotateX2 = rotate X axis in right direction
            // 2: rotateZ2 = rotate Z axis in left direction
            // 3: rotateZ1 = rotate Z axis in right direction
            // 4: go to nullposition
            // 5: snapshot
            // 6: start calibration
            // 7: uebernehme calibration
            QVariant selectedBox = rootObject->findChild<QObject*>("tabPositionGroup")->property("activeIndex");
            QVariant degree;
            bool isNumeric;
            float degreeNum;
            _command_data program;
            _command_data command;


            if(selectedBox.toInt() == 0)
            {
                degree = rootObject->findChild<QObject*>("rotateX1TextField")->property("text");
                degreeNum = degree.toFloat(&isNumeric);
                if(isNumeric)
                {
                    program = panoramaplanung->goHorizontalLeft(degreeNum);
                }
            }
            else if(selectedBox.toInt() == 1)
            {
                degree = rootObject->findChild<QObject*>("rotateX2TextField")->property("text");
                degreeNum = degree.toFloat(&isNumeric);
                if(isNumeric)
                {
                    program = panoramaplanung->goHorizontalRight(degreeNum);
                }
            }
            else if(selectedBox.toInt() == 2)
            {
                degree = rootObject->findChild<QObject*>("rotateZ2TextField")->property("text");
                degreeNum = degree.toFloat(&isNumeric);
                if(isNumeric)
                {
                    program = panoramaplanung->goVerticalLeft(degreeNum);
                }
            }
            else if(selectedBox.toInt() == 3)
            {
                degree = rootObject->findChild<QObject*>("rotateZ1TextField")->property("text");
                degreeNum = degree.toFloat(&isNumeric);
                if(isNumeric)
                {
                    program = panoramaplanung->goVerticalRight(degreeNum);
                }
            }
            else if(selectedBox.toInt() == 4)
            {
                program = panoramaplanung->setCamera();

            }
            else if(selectedBox.toInt() == 5)
            {
                program = panoramaplanung->triggerCamera();
            }
            else if(selectedBox.toInt() == 6)
            {
                program = panoramaplanung->fahreAufSensorBeginneZaehlen();
            }
            else if(selectedBox.toInt() == 7)
            {
                program = panoramaplanung->uebernehmeOffsetStoppZaehlen();
            }

            command = panoramaplanung->startPanoramapgram();
            communication->prepareProzessSendCmnd(program, command);

        }else{
            showMessageDialog(0);
        }
    }else{
        showMessageDialog(0);

    }
}

void MainWidget::quit()
{
#ifdef ANDROID
    rfcomm.disconnect();
#endif
}

void MainWidget::connectionStatusBlink()
{

    if(communication->getConnection())
    {
        timer = new QTimer(this);
        connect(this->timer, SIGNAL(timeout()), this, SLOT(periodialCheckStatusRun()));
        timer->start();
        timer->setInterval(3000);
        // select the recognized camera
        //QMetaObject::invokeMethod(rootObject->findChild<QObject*>("cameraComboBox"), "selectRecognizedCamera",
        //                          Q_ARG(QVariant, communication->getPanoProcessData().deviceData));
        timerConnectToPanoramaHead->stop();

    }
    else
    {
        if(blinkStatus)
        {
            switchConnectionStatus(RED_GREEN);
        }
        else
        {
            switchConnectionStatus(GREEN_RED);
        }
        blinkStatus = !blinkStatus;
    }

}

void MainWidget::periodialCheckStatusRun()
{

    if(communication->getConnection())
    {
        switchConnectionStatus(GREEN);
        if(recognizedAndChooseCamera)
        {
            bool changed = selectRecognizedCamera(communication->getPanoProcessData().deviceData);
            if(changed)
            {
                recognizedAndChooseCamera = false;
            }
        }
    }
    else
    {
        disconnectingProcedure();
        return;
    }

    communication->connectionBuffer();

}

void MainWidget::checkProgressStatus()
{
    // is panoramahead busy
    // then set busyProgressbar
    if(communication->getPanoProcessData().workToDo == 101)
    {
        activateLoadingIndicator(true);
    }
    // else
    // stop busyProgressbar == 102
    else{
        activateLoadingIndicator(false);
    }

    setCameraInformationTextInDialog(communication->getPanoProcessData().deviceData);
    /*
        // calculate percent of command and refresh progressbar
        float currentStep = communication->getPanoProcessData().current_step.toFloat();
        float totalSteps = communication->getPanoProcessData().total_steps.toFloat();
        float percent = currentStep/totalSteps;
        if(!isnan(percent)){
            qDebug() << "Prozent: " + QString::number(percent);
            setProgressValue(percent);
        }

        qDebug() << "DeviceData: " + communication->getPanoProcessData().deviceData;
*/

}

bool MainWidget::selectRecognizedCamera(QString cameraInfo)
{
    // camera is connected via usb
    if(communication->getPanoProcessData().statusStatusConnection){
        QStringList infoList = cameraInfo.split(";");
        if(infoList.size() > 3){
            QString model = infoList.at(1);
            // select the recognized camera
            return QMetaObject::invokeMethod(rootObject->findChild<QObject*>("cameraComboBox"), "selectRecognizedCamera",
                                             Q_ARG(QVariant, model));
        }
    }
    return false;
}

void MainWidget::setCameraInformationTextInDialog(QString cameraInfo)
{
    QString text;
    // camera is connected via usb
    if(communication->getPanoProcessData().statusStatusConnection){
        QStringList infoList = cameraInfo.split(";");
        if(infoList.size() > 3){
            QString manufacturer = infoList.at(0);
            QString model = infoList.at(1);
            QString deviceVers = infoList.at(2);
            QString serial = infoList.at(3);
            text = QString("Hersteller: ").append(manufacturer);
            text = text.append("\nModel: ").append(model);
            text = text.append("\nGeräteversion: ").append(deviceVers);
            text = text.append("\nSeriennummer:\n").append(serial);
        }
    }
    // camera is not connected via usb
    else
    {
        text = QString("Derzeit ist keine Kamera über USB angeschlossen.");
    }
    rootObject->findChild<QObject*>("cameraInformationDialog")->setProperty("text", text);
}

/**
 *  true: connection is OK => green
 *  false: connection is not OK => red
 *  @brief MainWidget::switchConnectionStatus
 *  @param status
 */
void MainWidget::switchConnectionStatus(ConnectionStatus status)
{

    switch(status)
    {
    case GREEN:
        rootObject->findChild<QObject*>("connectionGreen")->setProperty("opacity", "1");
        rootObject->findChild<QObject*>("connectionRed")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionRed_Green")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionGreen_Red")->setProperty("opacity", "0");
        break;
    case RED:
        rootObject->findChild<QObject*>("connectionRed")->setProperty("opacity", "1");
        rootObject->findChild<QObject*>("connectionGreen")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionRed_Green")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionGreen_Red")->setProperty("opacity", "0");
        break;
    case RED_GREEN:
        rootObject->findChild<QObject*>("connectionRed_Green")->setProperty("opacity", "1");
        rootObject->findChild<QObject*>("connectionGreen_Red")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionGreen")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionRed")->setProperty("opacity", "0");
        break;
    case GREEN_RED:
        rootObject->findChild<QObject*>("connectionGreen_Red")->setProperty("opacity", "1");
        rootObject->findChild<QObject*>("connectionRed_Green")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionGreen")->setProperty("opacity", "0");
        rootObject->findChild<QObject*>("connectionRed")->setProperty("opacity", "0");
        break;

    }
}

void MainWidget::disconnectingProcedure()
{
    // stop status blinking
    timerConnectToPanoramaHead->stop();

    if(communication->disconnectFromDevice())
    {
        timer->stop();
        checkProgress->stop();
    }
    connectionButton->setProperty("text","Verbinden");
    switchConnectionStatus(RED);

    activateLoadingIndicator(false);
}


void MainWidget::activateLoadingIndicator(bool active)
{
    bool is = QMetaObject::invokeMethod(rootObject->findChild<QObject*>("loadingIndicator"), "activateLoadingIndicator",
                                        Q_ARG(QVariant, active));
    if(active)
    {
        rootObject->findChild<QObject*>("loadingIndicator")->setProperty("opacity", 1);
    }
    else
    {
        rootObject->findChild<QObject*>("loadingIndicator")->setProperty("opacity", 0);
    }
    qDebug() << "activate Loading Indicator:" + is;
}


/**
 *  100 percent: 1
 *  0 percent: 0
 *  @brief MainWidget::setProgressValue
 *  @param value
 */
void MainWidget::setProgressValue(float value)
{
    qDebug() << "Progress: " << value;
    rootObject->findChild<QObject*>("progressBar")->setProperty("value", value);
}


/**
 *  visible: 1
 *  visible: 0
 *  @brief MainWidget::setOpacityOfProgressBar
 * @param value
 */
void MainWidget::setOpacityOfProgressBar(float value)
{
    rootObject->findChild<QObject*>("progressBar")->setProperty("opacity", value);
}

QString MainWidget::getStringFromUnsignedChar( unsigned char *str ){
    QString result = "";
    int lengthOfString = strlen( reinterpret_cast<const char*>(str) );

    // print string in reverse order
    QString s;
    for( int i = 0; i < lengthOfString; i++ ){
        s = QString( "%1" ).arg( str[i], 0, 16 );

        // account for single-digit hex values (always must serialize as two digits)
        if( s.length() == 1 )
            result.append( "0" );

        result.append( s );
    }

    return result;
}

/**
 * @brief MainWidget::showMessageDialog
 * @param error 0 means Bluetoothconnection is not available.
 *              1 means the value of focallength is missing.
 */
void MainWidget::showMessageDialog(int error)
{
    QString errorText;
    if(error == 0){
        errorText = "Bluetoothverbindung nicht vorhanden.";
    }else{
        errorText = "Bitte geben Sie einen Wert für die Brennweite ein.";
    }
    rootObject->findChild<QObject*>("messageDialog")->setProperty("text", errorText);
    rootObject->findChild<QObject*>("messageDialog")->setProperty("visible", true);
}
