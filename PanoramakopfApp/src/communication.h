#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#ifdef ANDROID
   #include "bluetooth/androidrfcomm.h"
   #include <QBluetoothLocalDevice>
#endif
#include "osdefine.h"
#include <QDebug>
#include <QObject>
#include "protocoldefs.h"
#include <QTime>
#include "time_delta.h"
#include "planning/panoramaplanung.h"
//#include <QtCore>


// Information
struct _pano_process_data
{
    // Aufbau: x-x-3-40-Akt.Schritt-Max.Schritte-Nowcommand
    QString current_step;
    QString total_steps;
    QString current_command;

    // Aufbau: x-x-3-x-41-stateConnected-DeviceData
    int deviceStateConnection = 0;
    QString deviceData = "Benutzerdefiniert";

    // Aufbau: x-x-3-x-42-stateConnected-Capture
    int statusStatusConnection = 0;
    int capture = 0;

    int workToDo = 102;


    void setPanoramaWorkData(unsigned char data[])
    {
        workToDo = QString::number(data[5]).toInt();
        qDebug() << "workToDo: " + workToDo;
    }

    void setCameraStatusData(unsigned char data[])
    {
        statusStatusConnection = QString::number(data[5]).toInt();
        capture = QString::number(data[6]).toInt();
    }

    void setData(unsigned char data[])
    {
        clear();
        current_step.append(QString::number(data[5]));
        total_steps.append(QString::number(data[6]));
        current_command.append(QString::number(data[7]));
    }

    void clear()
    {
        current_step.clear();
        total_steps.clear();
        current_command.clear();
    }

    void setCameraDeviceData(unsigned char data[])
    {
        deviceStateConnection = QString::number(data[5]).toInt();
        deviceData.clear();

        for(int i = 6; i < data[3]+3; i++)
        {
            deviceData.append(data[i]);
        }
    }

};

class Communication : public QObject
{
    Q_OBJECT

public:
    Communication(QObject* parent = 0);

private:
    static unsigned char buffer[1+8+240+1];
    static unsigned char bufferlength;
    static void packinbuffer(unsigned char inval);

    QTime deltaTime1;
    // timeoutdefinition for connection
    const double TIMEOUT = 5;
    bool connected;
#ifdef ANDROID
    QBluetoothLocalDevice localDevice;
#endif


public slots:
    void connectionBuffer();





public:
    Q_INVOKABLE void connectToDevice();

#ifdef ANDROID
    AndroidRfComm *rfcomm;
#endif

public:
    static unsigned char *package_BT_message(const unsigned char *inptr, unsigned char inlength, unsigned char channel);
    static bool unpackage_BT_message(unsigned char *inptr, unsigned char inlength);
    static unsigned char *getbufferptr();
    unsigned char getbufferlength();
    void send(unsigned char buffer[]);
    QByteArray receiveData();
    bool sendData(QByteArray data);

    _pano_process_data pano_process_data;
    _pano_process_data getPanoProcessData();

    bool getConnection();
    bool disconnectFromDevice();

    void prepareProzessSendCmnd(_command_data, _command_data);
    bool isConnectionEstablished();

};
#endif // COMMUNICATION_H
